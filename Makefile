CONTAINER = sdk.core.minespider.com
NAMESPACE = itminespider
IMAGE = $(NAMESPACE)/$(CONTAINER)
VERSION = $(shell git describe --tags --abbrev=0)
TAG = $(shell echo $(VERSION) | sed 's/^v//')
REGISTRY = cloud.canister.io:5000

aws-login:
	$(aws ecr get-login --no-include-email --region eu-central-1)

build:
	DOCKER_BUILDKIT=1 docker build --target release --tag $(REGISTRY)/$(IMAGE):latest --ssh default --build-arg APP_VERSION=$(VERSION) .

tag:
	docker tag $(REGISTRY)/$(IMAGE):latest $(REGISTRY)/$(IMAGE):$(TAG)

push-tag:
	docker push $(REGISTRY)/$(IMAGE):$(TAG)

push-latest:
	docker push $(REGISTRY)/$(IMAGE):latest

push-all: push-latest push-tag

unit-test:
	DOCKER_BUILDKIT=1 docker build --target development --tag $(REGISTRY)/$(IMAGE):localdev --ssh default --build-arg APP_VERSION=$(VERSION) .
	docker run --rm -it --init -v $(PWD):/app $(REGISTRY)/$(IMAGE):localdev jest

build-app:
	DOCKER_BUILDKIT=1 docker build --target development --tag $(REGISTRY)/$(IMAGE):localdev --ssh default --build-arg APP_VERSION=$(VERSION) .
	docker run --rm -it --init -v $(PWD):/app $(REGISTRY)/$(IMAGE):localdev yarn run build

publish:
	npm login
	npm publish --access public