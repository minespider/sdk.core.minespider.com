import { EncryptionService } from "../../../src/domain/EncryptionService";
import {
  PrivateLayer,
  PrivateLayerFiles,
} from "../../../src/domain/PrivateLayer";

const encryptionService = new EncryptionService();

it("should create the private layer without metadata", async () => {
  const content = Buffer.from("file content");
  const fileReference = {
    checksum: encryptionService.hash(content),
    name: "document.txt",
    type: "text/plain",
  };

  const privateFiles: PrivateLayerFiles = {};
  privateFiles[fileReference.checksum.toString()] = fileReference;

  const privateLayer = new PrivateLayer({
    metadata: {},
    files: privateFiles,
  });

  expect(Object.keys(privateLayer.props.files).length).toBeGreaterThan(0);
  expect(Object.keys(privateLayer.props.metadata).length).toBe(0);

  const encrypted = await encryptionService.encryptWithPassword(
    Buffer.from(JSON.stringify(privateLayer.toDTO())),
    "password"
  );

  const decrypted = await encryptionService.decryptWithPassword(
    encrypted,
    "password"
  );
  const parsed = JSON.parse(decrypted.toString());

  const reconstitutedPrivateLayer = new PrivateLayer({
    metadata: parsed.metadata,
    files: parsed.files,
  });

  expect(reconstitutedPrivateLayer).toStrictEqual(privateLayer);
});

it("should create the private layer without files", async () => {
  const privateLayer = new PrivateLayer({
    metadata: {
      key: "value",
    },
    files: {},
  });

  expect(Object.keys(privateLayer.props.files).length).toBe(0);
  expect(privateLayer.props.metadata["key"]).toBe("value");
});
