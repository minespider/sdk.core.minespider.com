import { EncryptionService } from "../../../src/domain/EncryptionService";
import { InMemoryBus } from "../../../src/infra/application/InMemoryBus";
import {
  TransparencyLayer,
  TransparencyLayerFiles,
} from "../../../src/domain/TransparencyLayer";
import { CertificateId } from "../../../src/domain/CertificateId";
import { UniqueDomainEntityID } from "../../../src/core/UniqueDomainEntityID";
import { GetCertificatePublicLayerQuery } from "../../../src/application/query/GetCertificatePublicLayerQuery";
import {
  CERTIFICATE_ACTIONS,
  CERTIFICATE_TYPES,
  PublicLayer,
} from "../../../src/domain/PublicLayer";
import { Result } from "../../../src/core/Result";
import { GetCertificateKeysLayerQuery } from "../../../src/application/query/GetCertificateKeysLayerQuery";
import { KeysLayer } from "../../../src/domain/KeysLayer";
import { GetFileQuery } from "../../../src/application/query/GetFileQuery";

const bus = new InMemoryBus();
const encryptionService = new EncryptionService();

it("should create the transparency layer", async () => {
  const transparencyLayerEncryptionKey = encryptionService.hash(
    Buffer.from("transparency layer key")
  );
  const sourceId = CertificateId.create(new UniqueDomainEntityID()).unwrap();
  bus.addUseCase(
    GetCertificatePublicLayerQuery.NAME,
    async (request: Request) => {
      return Result.ok(
        new PublicLayer({
          id: sourceId,
          parent: null,
          code: "4eccaa12813d",
          version: 0,
          owner: {
            id: "f3c44817-c51f-4662-9488-4eccaa12813d",
            publicKey: Buffer.from("public key"),
            keys: encryptionService.hash("1"),
          },
          recipient: {
            keys: encryptionService.hash("1"),
          },
          type: CERTIFICATE_TYPES.PRODUCT_PASSPORT,
          action: CERTIFICATE_ACTIONS.CREATED,
          metadata: {},
          materials: [
            {
              type: "Metal",
              taxonomy: "Gold",
              amount_type: "kg",
              amount: 100 * 1000,
            },
          ],
          privateLayerAddress: encryptionService.hash("2"),
          recipientLayerAddress: encryptionService.hash("3"),
          transparencyLayerAddress: encryptionService.hash("4"),
          linksLayerAddress: encryptionService.hash("5"),
          creationTimestamp: new Date().getTime(),
          files: {},
        })
      );
    }
  );

  bus.addUseCase(
    GetCertificateKeysLayerQuery.NAME,
    async (request: Request) => {
      return Result.ok(
        new KeysLayer(
          encryptionService.hash(Buffer.from("1")),
          encryptionService.hash(Buffer.from("2")),
          transparencyLayerEncryptionKey,
          encryptionService.hash(Buffer.from("3"))
        )
      );
    }
  );

  bus.addUseCase(GetFileQuery.NAME, async (request: Request) => {
    const fileContent = Buffer.from(
      JSON.stringify({
        metadata: {
          code: 12345,
        },
        files: {},
      })
    );
    const encryptedFileContent = await encryptionService.encryptWithPassword(
      fileContent,
      transparencyLayerEncryptionKey
    );

    return Result.ok(encryptedFileContent);
  });

  const content = Buffer.from("file content");
  const fileReference = {
    checksum: encryptionService.hash(content),
    name: "document.txt",
    type: "text/plain",
  };

  const transparencyFiles: TransparencyLayerFiles = {};
  transparencyFiles[fileReference.checksum.toString()] = fileReference;

  const transparencyLayer = new TransparencyLayer({
    id: sourceId.id.toString(),
    metadata: {
      abc: "def",
    },
    files: transparencyFiles,
    upstream: [],
  });

  expect(transparencyLayer.props.metadata["abc"]).toBe("def");
  expect(Object.keys(transparencyLayer.props.files).length).toBe(1);
});
