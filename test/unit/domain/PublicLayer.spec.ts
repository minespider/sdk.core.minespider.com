import { UniqueDomainEntityID } from "../../../src/core/UniqueDomainEntityID";
import { CertificateId } from "../../../src/domain/CertificateId";
import { EncryptionService } from "../../../src/domain/EncryptionService";
import { PrivateLayer } from "../../../src/domain/PrivateLayer";
import {
  CERTIFICATE_ACTIONS,
  CERTIFICATE_TYPES,
  PublicLayer,
} from "../../../src/domain/PublicLayer";
import { RecipientLayer } from "../../../src/domain/RecipientLayer";
import { KeysLayerFactory } from "../../../src/domain/KeysLayerFactory";
import { InMemoryBus } from "../../../src/infra/application/InMemoryBus";
import { TransparencyLayer } from "../../../src/domain/TransparencyLayer";
import { LinksLayer } from "../../../src/domain/LinksLayer";

const bus = new InMemoryBus();
const encryptionService = new EncryptionService();
const keysLayerFactory = new KeysLayerFactory(encryptionService);

it("should create the public layer", async () => {
  const certificateId = CertificateId.create(
    new UniqueDomainEntityID("11966a1c-2b08-40f0-9f07-e538212024a2")
  ).unwrap();
  const ownerId = "c03dd006-916a-4f71-9774-314034239a8e";
  const recipientLayer = new RecipientLayer(
    "3ab0fd5c-3052-4a88-a98d-f723f13b5216",
    Buffer.from("abc")
  );
  const recipientLayerHash = encryptionService.hash(
    JSON.stringify(recipientLayer.toDTO())
  );

  const privateLayer = new PrivateLayer({
    metadata: {},
    files: {},
  });
  const privateLayerHash = encryptionService.hash(
    JSON.stringify(privateLayer.toDTO())
  );

  const transparencyLayer = new TransparencyLayer({
    id: null,
    metadata: {},
    files: {},
    upstream: [],
  });
  const transparencyLayerHash = encryptionService.hash(
    JSON.stringify(transparencyLayer.toDTO())
  );

  const linksLayer = new LinksLayer({
    links: [],
    hash: encryptionService.hash([]),
  });

  const linksLayerHash = encryptionService.hash(linksLayer);

  const keysLayer = await keysLayerFactory.create();
  const keysLayerHash = encryptionService.hash(
    JSON.stringify(keysLayer.toDTO())
  );

  const publicLayer = new PublicLayer({
    id: certificateId,
    code: "e538212024a2",
    version: 0,
    type: CERTIFICATE_TYPES.PRODUCT_PASSPORT,
    action: CERTIFICATE_ACTIONS.CREATED,
    owner: {
      id: ownerId,
      publicKey: Buffer.from("public key"),
      keys: keysLayerHash,
    },
    parent: null,
    metadata: {
      example: "something",
    },
    materials: [
      {
        type: "Metal",
        taxonomy: "Gold",
        amount_type: "kg",
        amount: 100 * 1000,
      },
      {
        type: "Plastic",
        taxonomy: "PVC",
        amount_type: "kg",
        amount: 200 * 1000,
      },
    ],
    creationTimestamp: new Date().getTime(),
    privateLayerAddress: privateLayerHash,
    recipientLayerAddress: recipientLayerHash,
    transparencyLayerAddress: transparencyLayerHash,
    linksLayerAddress: linksLayerHash,
    files: {},
  });

  expect(publicLayer.id.id.toString()).toBe(
    "11966a1c-2b08-40f0-9f07-e538212024a2"
  );
  expect(publicLayer.type).toBe("PRODUCT_PASSPORT");
  expect(publicLayer.metadata["example"]).toBe("something");
});
