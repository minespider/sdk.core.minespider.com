import { EncryptionService } from "../../../src/domain/EncryptionService";
import { RecipientLayer } from "../../../src/domain/RecipientLayer";

const encryptionService = new EncryptionService();

it("should create the recipient layer", async () => {
  const recipientLayer = new RecipientLayer(
    "3ab0fd5c-3052-4a88-a98d-f723f13b5216",
    Buffer.from("abc")
  );

  expect(recipientLayer.id).toBe("3ab0fd5c-3052-4a88-a98d-f723f13b5216");

  const encrypted = await encryptionService.encryptWithPassword(
    Buffer.from(JSON.stringify(recipientLayer.toDTO())),
    "password"
  );

  const decrypted = await encryptionService.decryptWithPassword(
    encrypted,
    "password"
  );
  const parsed = JSON.parse(decrypted.toString());

  const reconstitutedRecipientLayer = new RecipientLayer(
    parsed.id,
    Buffer.from(parsed.publicKey)
  );

  expect(reconstitutedRecipientLayer).toStrictEqual(recipientLayer);
});
