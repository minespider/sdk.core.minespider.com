import { InMemoryBus } from "../../../src/infra/application/InMemoryBus";
import { Result } from "../../../src/core/Result";
import { KeysLayerFactory } from "../../../src/domain/KeysLayerFactory";
import { EncryptionService } from "../../../src/domain/EncryptionService";
import { GetCertificatePublicLayerQuery } from "../../../src/application/query/GetCertificatePublicLayerQuery";
import {
  CERTIFICATE_ACTIONS,
  CERTIFICATE_TYPES,
  PublicLayer,
} from "../../../src/domain/PublicLayer";
import { CertificateId } from "../../../src/domain/CertificateId";
import { UniqueDomainEntityID } from "../../../src/core/UniqueDomainEntityID";
import { GetCertificateKeysLayerQuery } from "../../../src/application/query/GetCertificateKeysLayerQuery";
import { KeysLayer } from "../../../src/domain/KeysLayer";

const bus = new InMemoryBus();
const encryptionService = new EncryptionService();

it("should create the keys layer when there is no source", async () => {
  const keysLayerFactory = new KeysLayerFactory(encryptionService);
  const keysLayer = await keysLayerFactory.create();

  expect(keysLayer.recipientEncryptionKey.toString().length).toBeGreaterThan(
    0
  );
  expect(keysLayer.privateEncryptionKey.toString().length).toBeGreaterThan(0);
  expect(
    keysLayer.transparencyEncryptionKey.toString().length
  ).toBeGreaterThan(0);
});

it("should create the keys layer when there are multiple sources", async () => {
  const sourceId = CertificateId.create(new UniqueDomainEntityID()).unwrap();
  bus.addUseCase(
    GetCertificatePublicLayerQuery.NAME,
    async (request: Request) => {
      return Result.ok(
        new PublicLayer({
          id: CertificateId.create(new UniqueDomainEntityID()).unwrap(),
          code: "f4def92ee5df",
          version: 0,
          parent: null,
          owner: {
            id: "621d7663-888d-4c0d-a663-f4def92ee5df",
            publicKey: Buffer.from("public key"),
            keys: encryptionService.hash("1"),
          },
          recipient: {
            keys: encryptionService.hash("1"),
          },
          type: CERTIFICATE_TYPES.PRODUCT_PASSPORT,
          action: CERTIFICATE_ACTIONS.CREATED,
          metadata: {},
          materials: [
            {
              type: "Metal",
              taxonomy: "Gold",
              amount_type: "kg",
              amount: 100 * 1000,
            },
            {
              type: "Plastic",
              taxonomy: "PVC",
              amount_type: "kg",
              amount: 200 * 1000,
            },
          ],
          privateLayerAddress: encryptionService.hash("2"),
          recipientLayerAddress: encryptionService.hash("3"),
          transparencyLayerAddress: encryptionService.hash("4"),
          linksLayerAddress: encryptionService.hash("5"),
          creationTimestamp: new Date().getTime(),
          files: {},
        })
      );
    }
  );

  bus.addUseCase(
    GetCertificateKeysLayerQuery.NAME,
    async (request: Request) => {
      return Result.ok(
        new KeysLayer(
          encryptionService.hash(Buffer.from("1")),
          encryptionService.hash(Buffer.from("2")),
          encryptionService.hash(Buffer.from("3")),
          encryptionService.hash(Buffer.from("4"))
        )
      );
    }
  );

  const keysLayerFactory = new KeysLayerFactory(new EncryptionService());
  const keysLayer = await keysLayerFactory.create();

  expect(keysLayer.recipientEncryptionKey.toString().length).toBeGreaterThan(
    0
  );
  expect(keysLayer.privateEncryptionKey.toString().length).toBeGreaterThan(0);
  expect(
    keysLayer.transparencyEncryptionKey.toString().length
  ).toBeGreaterThan(0);
  expect(keysLayer.linksKey.toString().length).toBeGreaterThan(0);

  const encrypted = await encryptionService.encryptWithPassword(
    Buffer.from(JSON.stringify(keysLayer.toDTO())),
    "password"
  );

  const decrypted = await encryptionService.decryptWithPassword(
    encrypted,
    "password"
  );
  const parsed = JSON.parse(decrypted.toString());

  const reconstitutedKeysLayer = new KeysLayer(
    parsed.recipient.encryption_key,
    parsed.private.encryption_key,
    parsed.transparency.encryption_key,
    parsed.links.encryption_key
  );

  expect(reconstitutedKeysLayer).toStrictEqual(keysLayer);
});
