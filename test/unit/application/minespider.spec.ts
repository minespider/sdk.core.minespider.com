import "dotenv/config";
import * as certificate from "./certificate.json";
import { CreateProductPassportProps } from "../../../src/application/command/CreateProductPassportCommand";
import { CertificateId, Minespider, UniqueDomainEntityID } from "../../../src/index";
import { Result } from "../../../src/core/Result";
import { EthereumAccount } from "../../../src/core/EthereumAccount";
import { EncryptionService } from "../../../src/domain/EncryptionService";

export enum VisibilityLayer {
  transparent = "transparent",
  private = "private",
  public = "public",
}

const prepareBlockchainCertificate = async (
  certificateDTO: any,
  certificateId: string
): Promise<Result<CreateProductPassportProps>> => {
  // FILES
  const attachments = certificateDTO.attachments;
  const publicFilesList = attachments.filter((item: any) => {
    return item.visibilityLayer == VisibilityLayer.public;
  });
  const privateFilesList = attachments.filter((item: any) => {
    return item.visibilityLayer == VisibilityLayer.private;
  });
  const transparencyFilesList = attachments.filter((item: any) => {
    return item.visibilityLayer == VisibilityLayer.transparent;
  });

  // METADATA
  const publicMetadata = certificateDTO.metadata.filter((item: any) => {
    return item.visibilityLayer == VisibilityLayer.public;
  });
  const privateMetadata = certificateDTO.metadata.filter((item: any) => {
    return item.visibilityLayer == VisibilityLayer.private;
  });
  const transparentMetadata = certificateDTO.metadata.filter((item: any) => {
    return item.visibilityLayer == VisibilityLayer.transparent;
  });

// LINKS
//@ts-ignore
  const links: CertificateLink[] = certificateDTO.links
    .map((linkDTO: any) => ({
      id: linkDTO.targetCertId,
      undisclosed: linkDTO.undisclosed
    }));

  if (!certificateDTO.ownerEntity.entityBlockchainAddress) {
    return Result.fail(new Error("Owner entity blockchain address is required"));
  }

  if (!certificateDTO.ownerEntity.encryptedMnemonicPhrase) {
    return Result.fail(new Error("Owner entity mnemonic phrase is required"));
  }

// MATERIALS
  const payload: CreateProductPassportProps = {
    public: {
      id: CertificateId.create(new UniqueDomainEntityID(certificateId)).unwrap(),
      code: certificateDTO.code,
      version: certificateDTO.version,
      parent:
        certificateDTO.parent && certificateDTO.parent.blockchainPublicLayerHash
          ? certificateDTO.parent.blockchainPublicLayerHash
          : null,
      metadata: publicMetadata,
      materials: [],
      files: publicFilesList
    },
    owner: {
      id: certificateDTO.ownerEntity.entityBlockchainAddress,
      //@ts-ignore
      encryptedMnemonicPhrase: certificateDTO.ownerEntity.encryptedMnemonicPhrase
    },
    transparency: {
      metadata: transparentMetadata,
      files: transparencyFilesList,
      //@ts-ignore
      tags: certificateDTO.tags
    },
    private: {
      metadata: privateMetadata,
      files: privateFilesList
    },
    links
  };

  return Result.ok(payload);
};

describe.skip("Minespider", function() {
  it("should publish test certificate", async () => {
    const sdk = new Minespider(
      String(process.env.MNSPDR_FILE_STORAGE_HOST),
      String(process.env.MNSPDR_NETWORK_HOST),
      String(process.env.MNSPDR_BLOCKCHAIN_ROOT_ACCOUNT_ADDRESS)
    );
    const rootEntityAccount = EthereumAccount.createFromMnemonic(process.env.MNSPDR_BLOCKCHAIN_ROOT_ACCOUNT_MNEMONIC as string).unwrap();
    const encryptionService = new EncryptionService();
    const ownerEntityAccounMnemonic = await encryptionService.decryptWithPrivateKey(
      Buffer.from(certificate.ownerEntity.encryptedMnemonicPhrase),
      rootEntityAccount.privateKey
    );
    const ownerEntityAccount = EthereumAccount.createFromMnemonic(ownerEntityAccounMnemonic.toString()).unwrap();
    // const publickLayer = await sdk.getPublicLayer("0xe9f7a965ec67f24cc64f8c6c2714e1169f2c8e02802c4c53d57b982421642183")
    // const editedKeysLayerDTO = await sdk.getKeysLayer(
    //   publickLayer.owner.keys,
    //   ownerEntityAccount.privateKey
    // );
    // const transaction = await sdk.getTransparencyLayer("0xba012ffe2f4d71ea04905d4dbdeba36fb2306591e4d6c22c33daa5eaff8ec82c", editedKeysLayerDTO.transparency.encryption_key)
    // console.log(transaction, publickLayer)
    const prepared: CreateProductPassportProps = Result.unwrap(await prepareBlockchainCertificate(certificate, certificate.id)) as CreateProductPassportProps;
    prepared.owner.privateKey = ownerEntityAccount.privateKey
    const created = await sdk.createProductPassport(prepared);
    expect(ownerEntityAccount).toBeTruthy();
    expect(prepared).toBeTruthy();
    expect(created).toBeTruthy();
    expect(certificate).toBeTruthy();
  });
});