import { FileStorageService } from "../../../src/infra/FileStorageService";
import * as nock from "nock";

const FILE_STORAGE_HOST = "http://files.minespider.com";

const fileStorageService = new FileStorageService(FILE_STORAGE_HOST);

describe("File Storage Service", function() {
  it("Should fail if file store responds with 500 code", async () => {
    nock(FILE_STORAGE_HOST)
      .post("/upload")
      .reply(500, "{}");

    const result = await fileStorageService.upload({
      checksum:
        "0x719accc61a9cc126830e5906f9d672d06eab6f8597287095a2c55a8b775e7016",
      name: "test1.txt",
      type: "plain/text",
      content: Buffer.from("abc123")
    });

    expect(result.error.message).toBe("Unable to upload file with name test1.txt :\nRequest failed with status code 500.");
    expect(result.isSuccess).toBe(false);
    expect(result.isFailure).toBe(true);
  });

  it("Should fail if file store responds with 400 code", async () => {
    nock(FILE_STORAGE_HOST)
      .post("/upload")
      .reply(400, "{}");

    const result = await fileStorageService.upload({
      checksum:
        "0x719accc61a9cc126830e5906f9d672d06eab6f8597287095a2c55a8b775e7016",
      name: "test1.txt",
      type: "plain/text",
      content: Buffer.from("abc123")
    });

    expect(result.error.message).toBe("Unable to upload file with name test1.txt :\nRequest failed with status code 400.");
    expect(result.isSuccess).toBe(false);
    expect(result.isFailure).toBe(true);
  });

  it("Should succeed if file store responds with 200 code and address", async () => {
    nock(FILE_STORAGE_HOST)
      .post("/upload")
      .reply(200, `{"address": "0x719accc61a9cc126830e5906f9d672d06eab6f8597287095a2c55a8b775e7016"}`);

    const result = await fileStorageService.upload({
      checksum:
        "0x719accc61a9cc126830e5906f9d672d06eab6f8597287095a2c55a8b775e7016",
      name: "test1.txt",
      type: "plain/text",
      content: Buffer.from("abc123")
    });

    expect(result.isSuccess).toBe(true);
    expect(result.isFailure).toBe(false);
  });

  it("Should succeed if file store responds with 200 code and content", async () => {
    nock(FILE_STORAGE_HOST)
      .post("/upload")
      .reply(200, `{"content": "0x719accc61a9cc126830e5906f9d672d06eab6f8597287095a2c55a8b775e7016"}`);

    const result = await fileStorageService.upload({
      checksum:
        "0x719accc61a9cc126830e5906f9d672d06eab6f8597287095a2c55a8b775e7016",
      name: "test1.txt",
      type: "plain/text",
      content: Buffer.from("abc123")
    });

    expect(result.isSuccess).toBe(true);
    expect(result.isFailure).toBe(false);
  });

  it("Should fail if file store responds with 200 code but without address or content", async () => {
    nock(FILE_STORAGE_HOST)
      .post("/upload")
      .reply(200, "{}");

    const result = await fileStorageService.upload({
      checksum:
        "0x719accc61a9cc126830e5906f9d672d06eab6f8597287095a2c55a8b775e7016",
      name: "test1.txt",
      type: "plain/text",
      content: Buffer.from("abc123")
    });

    expect(result.error.message).toBe("Unable to upload file with name test1.txt:\nNo address in response from File Storage.");
    expect(result.isSuccess).toBe(false);
    expect(result.isFailure).toBe(true);
  });

  it("Should succeed if file downloaded", async () => {
    const hash = "0x719accc61a9cc126830e5906f9d672d06eab6f8597287095a2c55a8b775e7016";
    nock(FILE_STORAGE_HOST)
      .get(`/download?filename=${hash}`)
      .reply(200, FILE_STORAGE_HOST);

    const result = await fileStorageService.download(hash);

    expect(result.isSuccess).toBe(true);
    expect(result.isFailure).toBe(false);
  });

  it("Should fail if file cannot be downloaded", async () => {
    const hash = "0x719accc61a9cc126830e5906f9d672d06eab6f8597287095a2c55a8b775e7016";
    nock(FILE_STORAGE_HOST)
      .get(`/download?filename=${hash}`)
      .reply(500, FILE_STORAGE_HOST);

    const result = await fileStorageService.download(hash);

    expect(result.error.message).toBe(`Unable to download data for hash ${hash}:\nRequest failed with status code 500`);
    expect(result.isSuccess).toBe(false);
    expect(result.isFailure).toBe(true);
  });
});