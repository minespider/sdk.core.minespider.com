import "dotenv/config";

import { v4 as uuid } from "uuid";
import * as ethers from "ethers";

import {
  CertificateId,
  InMemoryFile,
  Minespider,
  UniqueDomainEntityID
} from "../../src";
import { EthereumAccount } from "../../src/core/EthereumAccount";

it.skip("should edit a product passport", async () => {
  const certificateId = uuid();

  const sdk = new Minespider(
    String(process.env.MNSPDR_FILE_STORAGE_HOST),
    String(process.env.MNSPDR_NETWORK_HOST),
    String(process.env.MNSPDR_BLOCKCHAIN_CONTRACT_CERTIFICATE_ADDRESS)
  );

  const ownerWallet = ethers.Wallet.fromMnemonic(
    String(process.env.MNSPDR_BLOCKCHAIN_ROOT_ACCOUNT_MNEMONIC),
    "m/44'/60'/0'/0/0"
  );
  const ownerPrivateKey = ownerWallet.privateKey;
  const recipientWallet = ethers.Wallet.fromMnemonic(
    String(process.env.MNSPDR_BLOCKCHAIN_ROOT_ACCOUNT_MNEMONIC),
    "m/44'/60'/1'/0/0"
  );
  const recipientPublicKey = recipientWallet.publicKey;

  const file1: InMemoryFile = {
    checksum:
      "0x719accc61a9cc126830e5906f9d672d06eab6f8597287095a2c55a8b775e7016",
    name: "test1.txt",
    type: "plain/text",
    content: Buffer.from("abc123")
  };
  const file2: InMemoryFile = {
    checksum:
      "0xb1f1c74a1ba56f07a892ea1110a39349d40f66ca01d245e704621033cb7046a4",
    name: "test2.txt",
    type: "plain/text",
    content: Buffer.from("123abc")
  };
  const recipientId = uuid();
  const result = await sdk.createProductPassport({
    public: {
      id: CertificateId.create(
        new UniqueDomainEntityID(certificateId)
      ).unwrap(),
      version: 0,
      code: "jhgfdaajsdias",
      parent: null,
      metadata: {
        date: new Date().toString()
      },
      materials: [
        {
          type: "Metal",
          taxonomy: "Gold",
          amount_type: "kg",
          amount: 100
        }
      ],
      files: {}
    },
    recipient: {
      id: recipientId,
      publicKey: Buffer.from(recipientPublicKey)
    },
    owner: {
      id: ownerWallet.address,
      privateKey: Buffer.from(ownerPrivateKey)
    },
    transparency: {
      metadata: {},
      files: {
        [file1.checksum]: new Promise<InMemoryFile>((resolve) => {
          resolve(file1);
        })
      }
    },
    private: {
      metadata: {},
      files: {
        [file2.checksum]: file2
      }
    },
    links: []
  });

  const certificateDTO = result.certificate;
  expect(typeof result.transaction.blockHash).toBe("string");
  expect(typeof result.transaction.transactionHash).toBe("string");

  const publicLayerDTO = await sdk.getPublicLayer(
    certificateDTO.layers.public
  );

  const editedCertificateId = CertificateId.create(
    new UniqueDomainEntityID()
  ).unwrap();
  const editedCertificate = await sdk.editCertificate(
    {
      parentPublicLayer: certificateDTO.layers.public,
      parentKeysLayerHash: publicLayerDTO.owner.keys,
      public: {
        id: editedCertificateId,
        code: "fkmoerreamsioo",
        version: 1,
        metadata: {
          date: new Date().toString()
        },
        materials: [
          {
            type: "Metal",
            taxonomy: "Gold",
            amount_type: "kg",
            amount: 200
          }
        ],
        files: {}
      },
      transparency: {
        metadata: {
          log: true
        },
        files: {}
      },
      private: {
        metadata: {},
        files: {}
      },
      links: []
    },
    EthereumAccount.create(ownerPrivateKey).unwrap()
  );

  const editedPublicLayerDTO = await sdk.getPublicLayer(
    editedCertificate.certificate.layers.public
  );

  const editedKeysLayerDTO = await sdk.getKeysLayer(
    editedPublicLayerDTO.owner.keys,
    Buffer.from(ownerPrivateKey)
  );

  const editedTransparencyLayerDTO = await sdk.getTransparencyLayer(
    editedPublicLayerDTO.layers.transparency,
    editedKeysLayerDTO.transparency.encryption_key
  );

  // expect(editedTransparencyLayerDTO.history.length).toBe(2);
  // expect(editedTransparencyLayerDTO.history[0].from).toBe(publicLayerDTO.id);
  // expect(editedTransparencyLayerDTO.history[0].to).toBe(publicLayerDTO.id);
  // expect(editedTransparencyLayerDTO.history[1].from).toBe(publicLayerDTO.id);
  // expect(editedTransparencyLayerDTO.history[1].to).toBe(
  //   editedPublicLayerDTO.id
  // );

  try {
    await sdk.editCertificate(
      {
        parentPublicLayer: certificateDTO.layers.public,
        parentKeysLayerHash: publicLayerDTO.owner.keys,
        public: {
          id: editedCertificateId,
          code: "eordekowddma",
          version: 1,
          metadata: {
            date: new Date().toString()
          },
          materials: [
            {
              type: "Metal",
              taxonomy: "Gold",
              amount_type: "kg",
              amount: 300
            }
          ],
          files: {}
        },
        transparency: {
          metadata: {
            log: true
          },
          files: {}
        },
        private: {
          metadata: {},
          files: {}
        },
        links: []
      },
      EthereumAccount.create(ownerPrivateKey).unwrap()
    );
  } catch (error) {
    expect(error).toBe("Only the owner can edit this certificate");
  }
});
