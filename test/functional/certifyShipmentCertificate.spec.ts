import "dotenv/config";

import * as ethers from "ethers";
import { v4 as uuid } from "uuid";

import {
  CertificateId,
  InMemoryFile,
  Minespider,
  UniqueDomainEntityID
} from "../../src";
import { EthereumAccount } from "../../src/core/EthereumAccount";

it.skip("should certify a product passport", async () => {
  const certificateId = uuid();

  const sdk = new Minespider(
    String(process.env.MNSPDR_FILE_STORAGE_HOST),
    String(process.env.MNSPDR_NETWORK_HOST),
    String(process.env.MNSPDR_BLOCKCHAIN_CONTRACT_CERTIFICATE_ADDRESS)
  );

  const ownerWallet = ethers.Wallet.fromMnemonic(
    String(process.env.MNSPDR_BLOCKCHAIN_ROOT_ACCOUNT_MNEMONIC),
    "m/44'/60'/0'/0/0"
  );
  const ownerPrivateKey = ownerWallet.privateKey;
  const recipientWallet = ethers.Wallet.fromMnemonic(
    String(process.env.MNSPDR_BLOCKCHAIN_ROOT_ACCOUNT_MNEMONIC),
    "m/44'/60'/1'/0/0"
  );
  const recipientPublicKey = recipientWallet.publicKey;
  const ownerAccount = EthereumAccount.create(
    ownerPrivateKey.toString()
  ).unwrap();
  const certifierAccount = EthereumAccount.create(
    recipientWallet.privateKey.toString()
  ).unwrap();

  const file1: InMemoryFile = {
    checksum:
      "0x719accc61a9cc126830e5906f9d672d06eab6f8597287095a2c55a8b775e7016",
    name: "test1.txt",
    type: "plain/text",
    content: Buffer.from("abc123")
  };
  const file2: InMemoryFile = {
    checksum:
      "0xb1f1c74a1ba56f07a892ea1110a39349d40f66ca01d245e704621033cb7046a4",
    name: "test2.txt",
    type: "plain/text",
    content: Buffer.from("123abc")
  };

  const result = await sdk.createProductPassport({
    public: {
      id: CertificateId.create(
        new UniqueDomainEntityID(certificateId)
      ).unwrap(),
      parent: null,
      version: 0,
      code: "jhgfdaajsdias",
      metadata: {
        date: new Date().toString()
      },
      materials: [
        {
          type: "Metal",
          taxonomy: "Gold",
          amount_type: "kg",
          amount: 100
        }
      ],
      files: {}
    },
    recipient: {
      id: recipientWallet.address,
      publicKey: Buffer.from(recipientPublicKey)
    },
    owner: {
      id: ownerWallet.address,
      privateKey: Buffer.from(ownerPrivateKey)
    },
    transparency: {
      metadata: {},
      files: {
        [file1.checksum]: new Promise<InMemoryFile>((resolve) => {
          resolve(file1);
        })
      }
    },
    private: {
      metadata: {},
      files: {
        [file2.checksum]: file2
      }
    },
    links: []
  });

  const certificateDTO = result.certificate;
  expect(typeof result.transaction.blockHash).toBe("string");
  expect(typeof result.transaction.transactionHash).toBe("string");

  const publicLayerDTO = await sdk.getPublicLayer(
    certificateDTO.layers.public
  );

  const certifierCertificateKeysLayerAddress = await sdk.grantAccessToCertificate(
    certificateDTO.layers.public,
    ownerAccount,
    {
      id: certifierAccount.props.address.toString(),
      publicKey: certifierAccount.publicKey.toString()
    }
  );

  const certifiedCertificate = await sdk.certifyCertificate({
    parentPublicLayer: certificateDTO.layers.public,
    parentKeysLayerHash: certifierCertificateKeysLayerAddress,
    newCertificateId: CertificateId.create(
      new UniqueDomainEntityID()
    ).unwrap(),
    newCertificateCode: "dsauhdsaujhdsau",
    certifierAccount
  });

  const certifiedPublicLayerDTO = await sdk.getPublicLayer(
    certifiedCertificate.layers.public
  );

  const certifiedKeysLayerDTO = await sdk.getKeysLayer(
    certifiedPublicLayerDTO.owner.keys,
    certifierAccount.privateKey
  );

  const certifiedTransparencyLayerDTO = await sdk.getTransparencyLayer(
    certifiedPublicLayerDTO.layers.transparency,
    certifiedKeysLayerDTO.transparency.encryption_key
  );

  expect(certifiedPublicLayerDTO.owner.id).toBe(recipientWallet.address);
  expect(certifiedTransparencyLayerDTO.upstream.length).toBe(0);
});
