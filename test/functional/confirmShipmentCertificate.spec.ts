import "dotenv/config";
import { v4 as uuid } from "uuid";
import * as ethers from "ethers";

import {
  CertificateId,
  InMemoryFile,
  Minespider,
  UniqueDomainEntityID
} from "../../src";
import { EthereumAccount } from "../../src/core/EthereumAccount";

it.skip("should confirm a product passport", async () => {
  const certificateId = uuid();

  const sdk = new Minespider(
    String(process.env.MNSPDR_FILE_STORAGE_HOST),
    String(process.env.MNSPDR_NETWORK_HOST),
    String(process.env.MNSPDR_BLOCKCHAIN_CONTRACT_CERTIFICATE_ADDRESS)
  );

  const ownerWallet = ethers.Wallet.fromMnemonic(
    String(process.env.MNSPDR_BLOCKCHAIN_ROOT_ACCOUNT_MNEMONIC),
    "m/44'/60'/0'/0/0"
  );
  const ownerPrivateKey = ownerWallet.privateKey;
  const recipientWallet = ethers.Wallet.fromMnemonic(
    String(process.env.MNSPDR_BLOCKCHAIN_ROOT_ACCOUNT_MNEMONIC),
    "m/44'/60'/0'/1/0"
  );
  const recipientPublicKey = recipientWallet.publicKey;

  const recipientAccount = EthereumAccount.create(
    recipientWallet.privateKey
  ).unwrap();

  const file1: InMemoryFile = {
    checksum:
      "0x719accc61a9cc126830e5906f9d672d06eab6f8597287095a2c55a8b775e7016",
    name: "test1.txt",
    type: "plain/text",
    content: Buffer.from("abc123")
  };
  const file2: InMemoryFile = {
    checksum:
      "0xb1f1c74a1ba56f07a892ea1110a39349d40f66ca01d245e704621033cb7046a4",
    name: "test2.txt",
    type: "plain/text",
    content: Buffer.from("123abc")
  };

  const result = await sdk.createProductPassport({
    public: {
      id: CertificateId.create(
        new UniqueDomainEntityID(certificateId)
      ).unwrap(),
      version: 0,
      code: "jhgfdaajsdias",
      parent: null,
      metadata: {
        date: new Date().toString()
      },
      materials: [
        {
          type: "Metal",
          taxonomy: "Gold",
          amount_type: "kg",
          amount: 100 * 1000
        }
      ],
      files: {}
    },
    recipient: {
      id: recipientWallet.address,
      publicKey: Buffer.from(recipientPublicKey)
    },
    owner: {
      id: ownerWallet.address,
      privateKey: Buffer.from(ownerPrivateKey)
    },
    transparency: {
      metadata: {},
      files: {
        [file1.checksum]: new Promise<InMemoryFile>((resolve) => {
          resolve(file1);
        })
      }
    },
    private: {
      metadata: {},
      files: {
        [file2.checksum]: file2
      }
    },
    links: []
  });

  const certificateDTO = result.certificate;
  expect(typeof result.transaction.blockHash).toBe("string");
  expect(typeof result.transaction.transactionHash).toBe("string");

  const publicLayerDTO = await sdk.getPublicLayer(
    certificateDTO.layers.public
  );

  if (!publicLayerDTO.recipient) {
    throw new Error("recipient is missing");
  }

  const ownerAccount = EthereumAccount.create(
    recipientWallet.privateKey
  ).unwrap();

  const [_, __, confirmedCertificate] = await sdk.confirmCertificate({
    parentPublicLayer: certificateDTO.layers.public,
    keysLayerAddress: publicLayerDTO.recipient.keys,
    newCertificateId: CertificateId.create(
      new UniqueDomainEntityID()
    ).unwrap(),
    newCertificateCode: "uherjnhbdsahu",
    confirmedByAccount: recipientAccount
  });

  const confirmedPublicLayerDTO = await sdk.getPublicLayer(
    confirmedCertificate.layers.public
  );

  const confirmedKeysLayerDTO = await sdk.getKeysLayer(
    confirmedPublicLayerDTO.owner.keys,
    Buffer.from(recipientWallet.privateKey)
  );

  const confirmedTransparencyLayerDTO = await sdk.getTransparencyLayer(
    confirmedPublicLayerDTO.layers.transparency,
    confirmedKeysLayerDTO.transparency.encryption_key
  );

  const confirmedPrivateLayerDTO = await sdk.getPrivateLayer(
    confirmedPublicLayerDTO.layers.private,
    confirmedKeysLayerDTO.private.encryption_key
  );
});
