import "dotenv/config";
import { v4 as uuid } from "uuid";
import * as ethers from "ethers";

import {
  CertificateId,
  InMemoryFile,
  Minespider,
  UniqueDomainEntityID
} from "../../src";

describe("Create Product Passport", () => {
  it("should create the product passport", async () => {
    const certificateId = uuid();

    const sdk = new Minespider(
      String(process.env.MNSPDR_FILE_STORAGE_HOST),
      String(process.env.MNSPDR_NETWORK_HOST),
      String(process.env.MNSPDR_BLOCKCHAIN_CONTRACT_CERTIFICATE_ADDRESS)
    );

    const ownerWallet = ethers.Wallet.fromMnemonic(
      String(process.env.MNSPDR_BLOCKCHAIN_ROOT_ACCOUNT_MNEMONIC),
      "m/44'/60'/0'/0/0"
    );
    const ownerPrivateKey = ownerWallet.privateKey;
    const recipientWallet = ethers.Wallet.fromMnemonic(
      String(process.env.MNSPDR_BLOCKCHAIN_ROOT_ACCOUNT_MNEMONIC),
      "m/44'/60'/1'/0/0"
    );
    const recipientPublicKey = recipientWallet.publicKey;
    const recipientPrivateKey = recipientWallet.privateKey;

    const file1: InMemoryFile = {
      checksum:
        "0x719accc61a9cc126830e5906f9d672d06eab6f8597287095a2c55a8b775e7016",
      name: "test1.txt",
      type: "plain/text",
      content: Buffer.from("abc123")
    };
    const file2: InMemoryFile = {
      checksum:
        "0xb1f1c74a1ba56f07a892ea1110a39349d40f66ca01d245e704621033cb7046a4",
      name: "test2.txt",
      type: "plain/text",
      content: Buffer.from("123abc")
    };
    const recipientId = uuid();
    const result = await sdk.createProductPassport({
      public: {
        id: CertificateId.create(
          new UniqueDomainEntityID(certificateId)
        ).unwrap(),
        version: 0,
        code: "jhgfdaajsdias",
        parent: null,
        metadata: {
          date: new Date().toString()
        },
        materials: [
          {
            type: "Metal",
            taxonomy: "Gold",
            amount_type: "kg",
            amount: 100
          }
        ],
        files: {}
      },
      recipient: {
        id: recipientId,
        publicKey: Buffer.from(recipientPublicKey)
      },
      owner: {
        id: ownerWallet.address,
        privateKey: Buffer.from(ownerPrivateKey)
      },
      transparency: {
        metadata: {},
        files: {
          [file1.checksum]: new Promise<InMemoryFile>((resolve) => {
            resolve(file1);
          })
        }
      },
      private: {
        metadata: {},
        files: {
          [file2.checksum]: file2
        }
      },
      links: []
    });

    const certificateDTO = result.certificate;

    expect(typeof result.transaction.blockHash).toBe("string");
    expect(typeof result.transaction.transactionHash).toBe("string");

    const publicLayerDTO = await sdk.getPublicLayer(
      certificateDTO.layers.public
    );

    // Owner Certificate keys
    const ownerKeysLayerDTO = await sdk.getKeysLayer(
      publicLayerDTO.owner.keys,
      Buffer.from(ownerPrivateKey)
    );

    const ownerTransparencyLayerDTO = await sdk.getTransparencyLayer(
      publicLayerDTO.layers.transparency,
      ownerKeysLayerDTO.transparency.encryption_key
    );

    const ownerPrivateLayerDTO = await sdk.getPrivateLayer(
      publicLayerDTO.layers.private,
      ownerKeysLayerDTO.private.encryption_key
    );

    const ownerTransparencyFile = await sdk.getTransparencyFile(
      ownerTransparencyLayerDTO.files[
        Object.keys(ownerTransparencyLayerDTO.files)[0]
        ].checksum,
      ownerKeysLayerDTO.transparency.encryption_key,
      publicLayerDTO.layers.transparency
    );
    expect(ownerTransparencyFile.content.toString()).toBe("abc123");

    const ownerPrivateFile = await sdk.getPrivateFile(
      ownerPrivateLayerDTO.files[Object.keys(ownerPrivateLayerDTO.files)[0]]
        .checksum,
      ownerKeysLayerDTO.private.encryption_key,
      publicLayerDTO.layers.private
    );
    expect(ownerPrivateFile.content.toString()).toBe("123abc");
    expect(publicLayerDTO.parent).toBe(null);

    const ownerRecipientLayerDTO = await sdk.getRecipientLayer(
      publicLayerDTO.layers.recipient,
      ownerKeysLayerDTO.recipient.encryption_key
    );

    expect(ownerRecipientLayerDTO.id).toBe(recipientId);

    // Recipient Certificate keys
    if (!publicLayerDTO.recipient) {
      throw new Error("Recipient is empty");
    }

    const recipientKeysLayerDTO = await sdk.getKeysLayer(
      publicLayerDTO.recipient.keys,
      Buffer.from(recipientPrivateKey)
    );

    const recipientTransparencyLayerDTO = await sdk.getTransparencyLayer(
      publicLayerDTO.layers.transparency,
      recipientKeysLayerDTO.transparency.encryption_key
    );

    const recipientPrivateLayerDTO = await sdk.getPrivateLayer(
      publicLayerDTO.layers.private,
      recipientKeysLayerDTO.private.encryption_key
    );

    const recipientTransparencyFile = await sdk.getTransparencyFile(
      recipientTransparencyLayerDTO.files[
        Object.keys(recipientTransparencyLayerDTO.files)[0]
        ].checksum,
      recipientKeysLayerDTO.transparency.encryption_key,
      publicLayerDTO.layers.transparency
    );

    expect(recipientTransparencyFile.content.toString()).toBe("abc123");

    const recipientPrivateFile = await sdk.getPrivateFile(
      recipientPrivateLayerDTO.files[
        Object.keys(recipientPrivateLayerDTO.files)[0]
        ].checksum,
      recipientKeysLayerDTO.private.encryption_key,
      publicLayerDTO.layers.private
    );
    expect(recipientPrivateFile.content.toString()).toBe("123abc");
    expect(publicLayerDTO.parent).toBe(null);

    const recipientRecipientLayerDTO = await sdk.getRecipientLayer(
      publicLayerDTO.layers.recipient,
      recipientKeysLayerDTO.recipient.encryption_key
    );

    expect(recipientRecipientLayerDTO.id).toBe(recipientId);
  });

  it.skip("should not create the product passport if the owner id is invalid", async () => {
    const certificateId = uuid();

    const sdk = new Minespider(
      String(process.env.MNSPDR_FILE_STORAGE_HOST),
      String(process.env.MNSPDR_NETWORK_HOST),
      String(process.env.MNSPDR_BLOCKCHAIN_CONTRACT_CERTIFICATE_ADDRESS)
    );

    const ownerWallet = ethers.Wallet.createRandom();
    const ownerPrivateKey = ownerWallet.privateKey;
    const recipientWallet = ethers.Wallet.createRandom();
    const recipientPublicKey = recipientWallet.publicKey;

    const recipientId = uuid();
    try {
      await sdk.createProductPassport({
        public: {
          id: CertificateId.create(
            new UniqueDomainEntityID(certificateId)
          ).unwrap(),
          code: "uherjnhbdsahu",
          version: 0,
          parent: null,
          metadata: {
            date: new Date().toString()
          },
          materials: [
            {
              type: "Metal",
              taxonomy: "Gold",
              amount_type: "kg",
              amount: 100
            }
          ],
          files: {}
        },
        recipient: {
          id: recipientId,
          publicKey: Buffer.from(recipientPublicKey)
        },
        owner: {
          id: ownerWallet.address,
          privateKey: Buffer.from(ownerPrivateKey)
        },
        transparency: {
          metadata: {},
          files: {}
        },
        private: {
          metadata: {},
          files: {}
        },
        links: []
      });
    } catch (error: any) {
      expect(error.message).toBe(
        "Owner id invalid for does not match the given private key"
      );
    }
  });
});