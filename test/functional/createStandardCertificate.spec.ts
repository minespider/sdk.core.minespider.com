import "dotenv/config";
import { v4 as uuid } from "uuid";
import * as ethers from "ethers";

import {
  CertificateId,
  InMemoryFile,
  Minespider,
  UniqueDomainEntityID
} from "../../src";

it.skip("should create the entity certificate", async () => {
  const ownerCertificateId = CertificateId.create(
    new UniqueDomainEntityID(uuid())
  ).unwrap();
  const ownerWallet = ethers.Wallet.fromMnemonic(
    String(process.env.MNSPDR_BLOCKCHAIN_ROOT_ACCOUNT_MNEMONIC),
    "m/44'/60'/0'/0/0"
  );

  const sdk = new Minespider(
    String(process.env.MNSPDR_FILE_STORAGE_HOST),
    String(process.env.MNSPDR_NETWORK_HOST),
    String(process.env.MNSPDR_BLOCKCHAIN_CONTRACT_CERTIFICATE_ADDRESS)
  );

  const file1: InMemoryFile = {
    checksum:
      "0x719accc61a9cc126830e5906f9d672d06eab6f8597287095a2c55a8b775e7016",
    name: "test1.txt",
    type: "plain/text",
    content: Buffer.from("abc123")
  };
  const file2: InMemoryFile = {
    checksum:
      "0xb1f1c74a1ba56f07a892ea1110a39349d40f66ca01d245e704621033cb7046a4",
    name: "test2.txt",
    type: "plain/text",
    content: Buffer.from("123abc")
  };
  const result = await sdk.createEntityCertificate({
    public: {
      id: ownerCertificateId,
      parent: null,
      version: 0,
      code: "jhgfdaajsdias",
      metadata: {
        date: new Date().toString()
      },
      materials: [
        {
          type: "Metal",
          taxonomy: "Gold",
          amount_type: "kg",
          amount: 100 * 1000
        }
      ],
      files: {}
    },
    owner: {
      id: ownerWallet.address,
      privateKey: Buffer.from(ownerWallet.privateKey)
    },
    transparency: {
      metadata: {},
      files: {
        [file1.checksum]: new Promise<InMemoryFile>((resolve) => {
          resolve(file1);
        })
      }
    },
    private: {
      metadata: {},
      files: {
        [file2.checksum]: file2
      }
    },
    links: []
  });

  const certificateDTO = result.certificate;
  expect(typeof result.transaction.blockHash).toBe("string");
  expect(typeof result.transaction.transactionHash).toBe("string");

  const publicLayerDTO = await sdk.getPublicLayer(
    certificateDTO.layers.public
  );

  const keysLayerDTO = await sdk.getKeysLayer(
    publicLayerDTO.owner.keys,
    Buffer.from(ownerWallet.privateKey)
  );

  const transparencyLayerDTO = await sdk.getTransparencyLayer(
    publicLayerDTO.layers.transparency,
    keysLayerDTO.transparency.encryption_key
  );

  const privateLayerDTO = await sdk.getPrivateLayer(
    publicLayerDTO.layers.private,
    keysLayerDTO.private.encryption_key
  );

  const transparencyFile = await sdk.getTransparencyFile(
    transparencyLayerDTO.files[Object.keys(transparencyLayerDTO.files)[0]]
      .checksum,
    keysLayerDTO.transparency.encryption_key,
    publicLayerDTO.layers.transparency
  );
  expect(transparencyFile.content.toString()).toBe("abc123");

  const privateFile = await sdk.getPrivateFile(
    privateLayerDTO.files[Object.keys(privateLayerDTO.files)[0]].checksum,
    keysLayerDTO.private.encryption_key,
    publicLayerDTO.layers.private
  );
  expect(privateFile.content.toString()).toBe("123abc");
  expect(publicLayerDTO.parent).toBe(null);

  const publicLayer = await sdk.getPublicLayerByCertificateId(
    certificateDTO.id
  );
  expect(publicLayer.id).toBe(publicLayerDTO.id);
});
