import "dotenv/config";
import { v4 as uuid } from "uuid";
import * as ethers from "ethers";

import { CertificateId, Minespider, UniqueDomainEntityID } from "../../src";
import { EthereumAccount } from "../../src/core/EthereumAccount";
import { EncryptionService } from "../../src/domain/EncryptionService";

const encryptionService = new EncryptionService();

it.skip("the history should be aggregated properly", async () => {
  const certificateId = uuid();

  const sdk = new Minespider(
    String(process.env.MNSPDR_FILE_STORAGE_HOST),
    String(process.env.MNSPDR_NETWORK_HOST),
    String(process.env.MNSPDR_BLOCKCHAIN_CONTRACT_CERTIFICATE_ADDRESS)
  );

  const ownerWallet = ethers.Wallet.fromMnemonic(
    String(process.env.MNSPDR_BLOCKCHAIN_ROOT_ACCOUNT_MNEMONIC),
    "m/44'/60'/0'/0/0"
  );
  const ownerPrivateKey = ownerWallet.privateKey;
  const ownerAccount = EthereumAccount.create(ownerPrivateKey).unwrap();

  const recipientId = uuid();
  const recipientWallet = ethers.Wallet.fromMnemonic(
    String(process.env.MNSPDR_BLOCKCHAIN_ROOT_ACCOUNT_MNEMONIC),
    "m/44'/60'/1'/0/0"
  );
  const recipientPublicKey = recipientWallet.publicKey;
  const recipientAccount = EthereumAccount.create(
    recipientWallet.privateKey
  ).unwrap();

  const certifierId = uuid();
  const certifierWallet = ethers.Wallet.createRandom();
  const certifierPublicKey = certifierWallet.publicKey;
  const certifierAccount = EthereumAccount.create(
    certifierWallet.privateKey
  ).unwrap();

  const thirdPartyEntityId = uuid();
  const thirdPartyWallet = ethers.Wallet.createRandom();
  const thirdPartyPublicKey = thirdPartyWallet.publicKey;
  const thirdPartyAccount = EthereumAccount.create(
    thirdPartyWallet.privateKey
  ).unwrap();

  const linkResult = await sdk.createProductPassport({
    public: {
      id: CertificateId.create(new UniqueDomainEntityID()).unwrap(),
      version: 0,
      code: "lkofbsuybdeao",
      parent: null,
      metadata: {
        date: new Date().toString()
      },
      materials: [
        {
          type: "Metal",
          taxonomy: "Gold",
          amount_type: "kg",
          amount: 100 * 1000
        }
      ],
      files: {}
    },
    recipient: {
      id: recipientId,
      publicKey: Buffer.from(recipientPublicKey)
    },
    owner: {
      id: ownerWallet.address,
      privateKey: Buffer.from(ownerPrivateKey)
    },
    transparency: {
      metadata: {},
      files: {}
    },
    private: {
      metadata: {},
      files: {}
    },
    links: []
  });

  const result = await sdk.createProductPassport({
    public: {
      id: CertificateId.create(
        new UniqueDomainEntityID(certificateId)
      ).unwrap(),
      version: 0,
      code: "abcdefghijq",
      parent: null,
      metadata: {
        date: new Date().toString()
      },
      materials: [
        {
          type: "Metal",
          taxonomy: "Gold",
          amount_type: "kg",
          amount: 100 * 1000
        }
      ],
      files: {}
    },
    recipient: {
      id: recipientId,
      publicKey: Buffer.from(recipientPublicKey)
    },
    owner: {
      id: ownerWallet.address,
      privateKey: Buffer.from(ownerPrivateKey)
    },
    transparency: {
      metadata: {},
      files: {}
    },
    private: {
      metadata: {},
      files: {}
    },
    links: [
      {
        id: linkResult.certificate.id.toString(),
        undisclosed: true
      }
    ]
  });
  const certificateDTO = result.certificate;
  expect(typeof result.transaction.blockHash).toBe("string");
  expect(typeof result.transaction.transactionHash).toBe("string");

  const certificatePublicLayerDTO = await sdk.getPublicLayer(
    certificateDTO.layers.public
  );

  if (!certificatePublicLayerDTO.recipient) {
    throw new Error();
  }

  const certificateKeysLayerDTO = await sdk.getKeysLayer(
    certificatePublicLayerDTO.owner.keys,
    Buffer.from(ownerAccount.privateKey)
  );
  const certificateTransparencyLayerDTO = await sdk.getTransparencyLayer(
    certificatePublicLayerDTO.layers.transparency,
    certificateKeysLayerDTO.transparency.encryption_key
  );
  expect(certificateTransparencyLayerDTO.upstream.length).toBe(1);

  const editedCertificate = await sdk.editCertificate(
    {
      parentPublicLayer: certificateDTO.layers.public,
      parentKeysLayerHash: certificatePublicLayerDTO.owner.keys,
      public: {
        id: CertificateId.create(new UniqueDomainEntityID()).unwrap(),
        version: 1,
        code: "abcdefghijq",
        metadata: {
          date: new Date().toString()
        },
        materials: [
          {
            type: "Metal",
            taxonomy: "Gold",
            amount_type: "kg",
            amount: 200 * 1000
          }
        ],
        files: {}
      },
      transparency: {
        metadata: {
          edited: true,
          purchase_timestamp: new Date().getTime()
        },
        files: {}
      },
      private: {
        metadata: {},
        files: {}
      },
      links: [
        {
          id: linkResult.certificate.id.toString(),
          undisclosed: true
        }
      ]
    },
    ownerAccount
  );
  const editedPublicLayerDTO = await sdk.getPublicLayer(
    editedCertificate.certificate.layers.public
  );

  if (!editedPublicLayerDTO.recipient) {
    throw new Error("recipient is missing");
  }

  const editedKeysLayerDTO = await sdk.getKeysLayer(
    editedPublicLayerDTO.owner.keys,
    Buffer.from(ownerAccount.privateKey)
  );
  const editedTransparencyLayerDTO = await sdk.getTransparencyLayer(
    editedPublicLayerDTO.layers.transparency,
    editedKeysLayerDTO.transparency.encryption_key
  );
  expect(editedTransparencyLayerDTO.upstream.length).toBe(2);
  const [__, ___, confirmedCertificate] = await sdk.confirmCertificate({
    parentPublicLayer: editedCertificate.certificate.layers.public,
    keysLayerAddress: editedPublicLayerDTO.recipient.keys,
    newCertificateId: CertificateId.create(
      new UniqueDomainEntityID()
    ).unwrap(),
    newCertificateCode: "poiuytewqlkjhgf",
    confirmedByAccount: recipientAccount
  });

  const thirdPartyCertificateKeysLayerAddress = await sdk.grantAccessToCertificate(
    confirmedCertificate.layers.public,
    recipientAccount,
    {
      id: thirdPartyEntityId,
      publicKey: thirdPartyPublicKey
    }
  );

  const [
    _,
    thirdPartyConfirmedKeysLayerAddress,
    thirdPartyConfirmedCertificate
  ] = await sdk.confirmCertificate({
    parentPublicLayer: confirmedCertificate.layers.public,
    keysLayerAddress: thirdPartyCertificateKeysLayerAddress,
    newCertificateId: CertificateId.create(
      new UniqueDomainEntityID()
    ).unwrap(),
    newCertificateCode: "lkjhgfdsuytrew",
    confirmedByAccount: thirdPartyAccount
  });

  const thirdPartyConfirmedKeysLayerDTO = await sdk.getKeysLayer(
    thirdPartyConfirmedKeysLayerAddress,
    Buffer.from(thirdPartyAccount.privateKey)
  );

  const thirdPartyConfirmedCertificatePrivateLayer = await sdk.getPrivateLayer(
    thirdPartyConfirmedCertificate.layers.private,
    thirdPartyConfirmedKeysLayerDTO.private.encryption_key
  );

  const certifierCertificateKeysLayerAddress = await sdk.grantAccessToCertificate(
    thirdPartyConfirmedCertificate.layers.public,
    thirdPartyAccount,
    {
      id: certifierId,
      publicKey: certifierPublicKey
    }
  );

  const certifiedCertificate = await sdk.certifyCertificate({
    parentPublicLayer: thirdPartyConfirmedCertificate.layers.public,
    parentKeysLayerHash: certifierCertificateKeysLayerAddress,
    newCertificateId: CertificateId.create(
      new UniqueDomainEntityID()
    ).unwrap(),
    newCertificateCode: "zxcvbnasdfghj",
    certifierAccount: certifierAccount
  });

  const certifiedPublicLayerDTO = await sdk.getPublicLayer(
    certifiedCertificate.layers.public
  );
  const certifiedKeysLayerDTO = await sdk.getKeysLayer(
    certifiedPublicLayerDTO.owner.keys,
    Buffer.from(certifierWallet.privateKey)
  );
  const certifiedTransparencyLayerDTO = await sdk.getTransparencyLayer(
    certifiedPublicLayerDTO.layers.transparency,
    certifiedKeysLayerDTO.transparency.encryption_key
  );
  expect(certifiedTransparencyLayerDTO.upstream.length).toBe(0);

  const certifiedLinksLayerDTO = await sdk.getLinksLayer(
    certifiedPublicLayerDTO.layers.links,
    certifiedKeysLayerDTO.links.encryption_key
  );

  expect(certifiedLinksLayerDTO.hash).toBe(
    encryptionService.hash(certifiedLinksLayerDTO.links)
  );
});
