## Minespider SDK 2.0 API

### Configuring the SDK

```typescript
const sdk = new Minespider(
  "https://file-storage.staging.minespider.com",
  "https://network.staging.minespider.com",
  "0x0C891ADBcE47AA89eFd4a1d115D778d7D5F3E897"
);

```

### How to generate the keccak256 for a specific layer

```typescript

const stringify = require('fast-json-stable-stringify');
const keccak256 = require('keccak256');

const hash = `0x${keccak256(stringify(layer.toDTO())).toString("hex")}`;

```

### Create Product Passport

```typescript
createProductPassport(
  props: CreateProductPassportProps
): Promise<CertificateDTO>;
```

<details>
<summary>More details</summary>
<p>

```typescript
interface CreateProductPassportProps {
  public: {
    id: CertificateId;
    parent: string | null;
    metadata: {
      [key: string]: string | number | boolean;
    };
    materials: {
      type: string;
      taxonomy: string;
      amount_type: string;
      amount: number;
    }[];
  };
  recipient: {
    id: RecipientId;
    publicKey: Buffer;
  };
  owner: {
    id: string;
    privateKey: Buffer;
  };
  transparency: {
    metadata: {
      [key: string]: string | number | boolean;
    };
    files: {
      [key: string]:
        | InMemoryFile
        | FileReference
        | Promise<InMemoryFile | FileReference>;
    };
    sources: {
      [key: string]: string;
    };
  };
  private: {
    metadata: {
      [key: string]: string | number | boolean;
    };
    files: {
      [key: string]:
        | InMemoryFile
        | FileReference
        | Promise<InMemoryFile | FileReference>;
    };
  };
}

```

</p>
</details>

---

### Create Entity Certificate

```typescript
createEntityCertificate(
  props: CreateEntityCertificateProps
): Promise<CertificateDTO>;
```

<details>
<summary>More details</summary>
<p>

```typescript
interface CreateEntityCertificateProps {
  public: {
    id: CertificateId;
    parent: string | null;
    metadata: {
      [key: string]: string | number | boolean;
    };
    materials: {
      type: string;
      taxonomy: string;
      amount_type: string;
      amount: number;
    }[];
  };
  owner: {
    id: string;
    privateKey: Buffer;
  };
  transparency: {
    metadata: {
      [key: string]: string | number | boolean;
    };
    files: {
      [key: string]:
        | InMemoryFile
        | FileReference
        | Promise<InMemoryFile | FileReference>;
    };
    sources: {
      [key: string]: string;
    };
  };
  private: {
    metadata: {
      [key: string]: string | number | boolean;
    };
    files: {
      [key: string]:
        | InMemoryFile
        | FileReference
        | Promise<InMemoryFile | FileReference>;
    };
  };
}
```

</p>
</details>

---

### Edit Certificate

```typescript
editCertificate(
  props: EditCertificateProps,
  ownerAccount: EthereumAccount
): Promise<CertificateDTO>;
```

<details>
<summary>More details</summary>
<p>

```typescript
interface EditCertificateProps {
  parentPublicLayer: string;
  parentKeysLayerHash: string;
  public: {
    id: CertificateId;
    metadata: {
      [key: string]: string | number | boolean;
    };
    materials: {
      type: string;
      taxonomy: string;
      amount_type: string;
      amount: number;
    }[];
  };
  transparency: {
    metadata: {
      [key: string]: string | number | boolean;
    };
    files: {
      [key: string]:
        | InMemoryFile
        | FileReference
        | Promise<InMemoryFile | FileReference>;
    };
    sources: {
      [key: string]: string;
    };
  };
  private: {
    metadata: {
      [key: string]: string | number | boolean;
    };
    files: {
      [key: string]:
        | InMemoryFile
        | FileReference
        | Promise<InMemoryFile | FileReference>;
    };
  };
}

```

</p>
</details>

---

### Confirm Certificate

```typescript
confirmCertificate(
  props: ConfirmCertificateProps
): Promise<ConfirmedCertificateDTO>;
```

<details>
<summary>More details</summary>
<p>

```typescript
interface ConfirmCertificateProps {
  parentPublicLayer: string;
  keysLayerAddress: string;
  newCertificateId: CertificateId;
  confirmedByAccount: EthereumAccount;
}

```

</p>
</details>

---

### Certify Certificate

```typescript
certifyCertificate(
  props: CertifyCertificateProps
): Promise<CertificateDTO>;
```

<details>
<summary>More details</summary>
<p>

```typescript
interface CertifyCertificateProps {
  parentPublicLayer: string;
  parentKeysLayerHash: string;
  newCertificateId: CertificateId;
  certificerAccount: EthereumAccount;
}

```

</p>
</details>

---

### Read Certificate's Public Layer

```typescript
getPublicLayer(
  publicLayerHash: string
): Promise<PublicLayerDTO>;
```

<details>
<summary>More details</summary>
<p>

```typescript
 interface PublicLayerDTO {
  id: string;
  type: string;
  parent: string | null;
  owner: {
    id: string;
    publicKey: string;
    keys: string;
  };
  recipient?: {
    keys: string;
  };
  certifiedBy?: {
    id: string;
    publicKey: string;
    keys: string;
    timestamp: number;
  };
  materials: {
    type: string;
    taxonomy: string;
    amount_type: string;
    amount: number;
  }[];
  metadata: {
    [key: string]: string | number | boolean;
  };
  layers: {
    recipient: string;
    transparency: string;
    private: string;
  };
  creation_timestamp: number;
}

```

</p>
</details>

---

### Read Certificate's Private Layer

```typescript
getPrivateLayer(
  privateLayerHash: string,
  privateLayerEncryptionKey: string
): Promise<PrivateLayerDTO>;
```

<details>
<summary>More details</summary>
<p>

```typescript
interface PrivateLayerDTO {
  metadata: {
    [key: string]: string | number | boolean;
  };
  files: {
    [key: string]: FileReference;
  };
  confirmedBy?: {
    id: string;
    publicKey: string;
    keys: string;
    timestamp: number;
  };
}

```

</p>
</details>

---

### Read Certificate's Transparency Layer

```typescript
getTransparencyLayer(
  transparencyLayerHash: string,
  transparencyLayerEncryptionKey: string
): Promise<TransparencyLayerDTO>
```

<details>
<summary>More details</summary>
<p>

```typescript
interface TransparencyLayerDTO {
  metadata: {
    [key: string]: string | number | boolean;
  };
  history: {
    from: string | null;
    to: string;
    hash: string;
  }[];
  files: {
    [key: string]: FileReference;
  };
  sources: {
    [key: string]: TransparencyLayerDTO;
  };
}

```

</p>
</details>

---

### Read Certificate's Recipient Layer

```typescript
getRecipientLayer(
  recipientLayerHash: string,
  recipientLayerEncryptionKey: string
): Promise<RecipientLayerDTO>
```

<details>
<summary>More details</summary>
<p>

```typescript
interface RecipientLayerDTO {
  id: string;
  publicKey: string;
}

```

---

### Read Certificate's Keys Layer

```typescript
getKeysLayer(
  keysLayerHash: string,
  privateKey: Buffer
): Promise<KeysLayerDTO>
```

<details>
<summary>More details</summary>
<p>

```typescript
interface KeysLayerDTO {
  recipient: {
    encryption_key: string;
  };
  private: {
    encryption_key: string;
  };
  transparency: {
    encryption_key: string;
    sources: {
      [key: string]: {
        encryption_key: string;
      };
    };
  };
}

```

</p>
</details>

---

### Download a File from Private Layer

```typescript
getPrivateFile(
  hash: string,
  encryptionKey: string,
  privateLayerHash: string
): Promise<InMemoryFile>
```

---

### Download a File from Transparency Layer

```typescript
getTransparencyFile(
  hash: string,
  encryptionKey: string,
  transparencyLayerHash: string
): Promise<InMemoryFile>
```

---

### Grant access to Certificate

```typescript
grantAccessToCertificate(
  publicLayerAddress: string,
  ownerAccount: EthereumAccount,
  grantAccessTo: {
    id: string;
    publicKey: string;
  }
): Promise<string>;
```

---
### Shared interfaces

<details>
<summary>More details</summary>
<p>

```typescript

interface CertificateDTO {
  id: string;
  layers: {
    public: string;
    recipient: string;
    transparency: string;
    private: string;
  };
}

interface EthereumAccount {
  mnemonic?: Buffer;
  address: Buffer;
  publicKey: Buffer;
  privateKey: Buffer;
}

interface FileReference {
  checksum: string;
  name: string;
  type: string;
}

interface InMemoryFile {
  checksum: string;
  name: string;
  type: string;
  content: Buffer;
}

interface ConfirmedCertificateDTO {
  keysLayerAddress: string,
  certificate: CertificateDTO
}

```

</p>
</details>
