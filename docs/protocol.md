# Minespider Protocol

The Minespider Protocol integrates the existing upstream due diligence solutions with an open protocol to
transmit data downstream beyond points of transformation to reach the companies who then benefit from
a secure raw material supply chain.
The Minespider Protocol is composed of encrypted certificates stored
in a decentralized Blockchain. The data is stored in different layers inside the certificate so that it can be transmitted safely down the supply chain guaranteeing the self-sovereignty.

### Blockchain

As defined on [Wikipedia](https://pt.wikipedia.org/wiki/Blockchain), a blockchain, originally block chain, is a growing list of records, called blocks, that are linked using cryptography. Each block contains a cryptographic hash of the previous block, a timestamp, and transaction data (generally represented as a Merkle tree).

By design, a blockchain is resistant to modification of the data. It is "an open, distributed ledger that can record transactions between two parties efficiently and in a verifiable and permanent way". For use as a distributed ledger, a blockchain is typically managed by a peer-to-peer network collectively adhering to a protocol for inter-node communication and validating new blocks. Once recorded, the data in any given block cannot be altered retroactively without alteration of all subsequent blocks, which requires consensus of the network majority. Although blockchain records are not unalterable, blockchains may be considered secure by design and exemplify a distributed computing system with high Byzantine fault tolerance. Decentralized consensus has therefore been claimed with a blockchain.

### Certificate

A Certificate is composed by some blocks of informations stored in different layers in order to increase the flexibility on safely sharing information between entities of the supply chain.
Certificates are read only, which means that any change to it results on a new certificate being generated with a proper history aggregation.

The certificate's main block of information is stored in the Blockchain, which consists of:
- Id: Unique identifier for the certificate
- Parent: The reference for the previous version of this certificate. Empty if not present
- Creation timestamp: Timestamp for when the Certificate was created
- Public layer: Reference for the Public layer
- Hash: The computed keccak256 hash of the Certificate information

#### Product Passports

Product passports have a "recipient" entity that have access to all the layers as the product passport owner has. This recipient can share the product passport with any other entity but can not make changes to the product passport.

#### Entity Certificates

The "recipient" of the Entity Certificates is the same as the owner. It can be shared with any entity, but only the owner can make changes to it (generating a new certificate).

### Layers

The layers of a certificate are files stored inside a file storage system that are encrypted in order to be read by other entities safely. 

#### Keys layer

This layer stores a set of symmetric encryption keys used to open the Private, Transparency and Recipient layers files. These encryption keys are generated randomically when creating a certificate and is encrypted using the public key of the entity that owns it.

#### Public Layer

The public layer is where the Certificate's public data can be accessed by anyone without the need of an encryption key. It is stored unencrypted and contains references for the other layers.

Data stored in the public layer:
- Type: Product Passport or Entity Certificate
- Parent: The reference for the previous version of this certificate, if present
- Owner: Information about which entity owns this Certificate
- Certifier: Information about which entity certified this Certificate, if present
- Materials: The list of materials and amounts comprised in the Certificate
- Metadata: Key-value store used to store non-structured data inside the Certificate
- Layers: References for Recipient, Transparency and Private layers
- Creation timestamp: Timestamp for when the Certificate was created

#### Private Layer

The private layer is meant to store data that will only be shared between the transaction parties (Owner and Recipient). When a new Certificate is created based on this one, this data is not carried over.
It is encrypted using a symmetric encryption key stored in the Keys layer.

Data stored in the private layer:
- Metadata: Key-value store used to store non-structured data inside the Certificate
- Files: References to private files attached. These files are also encrypted using the same symmetric encryption key as the Private Layer.
- Confirmed By: Information about which entity validated the data inside this Certificate. Any entity can confirm a certificate, as long as it has access to its Keys Layer.

#### Transparency Layer

The transparency layer is meant to store data that will be shared with other entities of the supply chain. When a new Certificate is created based on this one, this data is carried over.

Data stored in the transparency layer:
- Metadata: Key-value store used to store non-structured data inside the Certificate
- Files: References to private files attached. These files are also encrypted using the same symmetric encryption key as the Transparency Layer.
- History: A list of versions of a Certificate, containing computed hashes that can guarantee the integrity of the changes.
- Sources: When creating a Certificate, multiple certificates can be selected as "sources". The Transparency layer of those certificates are stored inside this field.

#### Recipient Layer

Due to the need for identity privacy on certificate transactions, the "recipient" of a certificate is encrypted in this layer so that only the entities with access to the encryption keys can read it.

#### Actions (Edit, Confirm, Certify)

##### Edit

The owner of a certificate can make changes to it, but the original certificate is read only, so when the edit action happens a new certificate is generated and an entry is added to the certificate's history.


##### Confirm

When an entity receives a certificate, it can "confirm" that the certificate is OK. When a confirm action happens, a new certificate is generated and an entry is added to the certificate's history. Any entity can confirm a certificate, as long as it has access to its Keys Layer.

##### Certify

A Certifier entity can "certify" a certificate. When doing so, a new certificate is generated and an entry is added to the certificate's history.
