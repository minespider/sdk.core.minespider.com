# Minespider SDK 2.0

The official Minespider SDK for Javascript, available for browsers and mobile devices, or Node.js backends.
For release notes, see the [CHANGELOG](https://gitlab.com/minespider/sdk.core.minespider.com/-/blob/master/CHANGELOG.md).

## Installing
### In the Browser
To use the SDK in the browser, simply add the following script tag to your HTML pages:
```
<script src="https://unpkg.com/@minespider/core-sdk@2.0.0-beta.0/dist/index.js"></script>
```
### In Node.js

The preferred way to install the Minespider SDK for Node.js is to use the [npm](http://npmjs.org/) package manager for Node.js. Simply type the following into a terminal window:

```
npm install @minespider/core-sdk
```

### Docs

- [Minespider Protocol](/docs/protocol.md)
- [SDK](/docs/sdk.md)
