const inquirer = require("inquirer");
const Wallet = require("ethereumjs-wallet");

const generate = async () => {
  const data = await inquirer.prompt([
    {
      name: "privateKey",
      type: "input",
      message: "Inform the private key (hex)",
    },
    {
      name: "password",
      type: "password",
      message: "Inform the password (hidden)",
    },
  ]);

  const key = Buffer.from(data.privateKey, "hex");
  const wallet = Wallet.fromPrivateKey(key);
  console.log(wallet.toV3String(data.password));
};

generate();
