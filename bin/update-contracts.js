const inquirer = require("../node_modules/inquirer");
const fs = require("fs");
const { promisify } = require("util");
const readDirAsync = promisify(fs.readdir);
const readFileAsync = promisify(fs.readFile);
const writeFileAsync = promisify(fs.writeFile);
const existsAsync = promisify(fs.exists);
const openAsync = promisify(fs.open);
const closeAsync = promisify(fs.close);
const unlinkAsync = promisify(fs.unlink);

const touchAsync = async (filePath) => {
  const fileExists = await existsAsync(filePath);

  if (fileExists) {
    await unlinkAsync(filePath);
  }

  await closeAsync(await openAsync(filePath, "w"));
};

async function updateContracts() {
  const path = await inquirer.prompt([
    {
      name: "origin",
      type: "input",
      message: "Inform the Minespider protocol folder",
      default:
        process.cwd() +
        "/../protocol.core.minespider.com/artifacts/contracts/",
    },
  ]);

  const originPath = path.origin;
  const destinationPath = process.cwd() + "/src/infra/blockchain/contracts/";
  const contracts = await readDirAsync(path.origin);

  for (contract of contracts) {
    const contractName = contract.replace(".sol", "");
    const contractFile = `${originPath}${contract}/${contractName}.json`;
    const contractDefinition = JSON.parse(await readFileAsync(contractFile));
    const abiFile = `${destinationPath}${contractName}.ts`;

    await touchAsync(abiFile);
    await writeFileAsync(
      abiFile,
      `const ${contractName}ABI = JSON.parse(\`${JSON.stringify(
        contractDefinition.abi
      )}\`);\nexport default ${contractName}ABI`
    );

    console.log(`ABI file generated at ${abiFile}`);
  }
}

updateContracts();
