const inquirer = require("inquirer");
const ethCrypto = require("eth-crypto");
const ethers = require("ethers");

async function getPrivateKey() {
  const input = await inquirer.prompt([
    {
      name: "mnemonic",
      type: "input",
      message: "Inform the mnemonic phrase",
    },
    {
      name: "index",
      type: "input",
      message: "Inform the index",
      default: 0,
    },
  ]);

  const wallet = ethers.Wallet.fromMnemonic(
    input.mnemonic,
    `m/44'/60'/${input.index}'/0/0`
  );

  console.log(
    JSON.stringify({
      address: wallet.address,
      privateKey: wallet.privateKey,
      publicKey: ethCrypto.publicKeyByPrivateKey(wallet.privateKey),
    })
  );
}

getPrivateKey();
