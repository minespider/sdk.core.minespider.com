const inquirer = require('inquirer');
const ethCrypto = require('eth-crypto');

async function getPublicKey() {
  const data = await inquirer.prompt([{
    name: 'privateKey',
    type: 'input',
    message: 'Inform the private key'
  }])

  const publicKey = ethCrypto.publicKeyByPrivateKey(data.privateKey)
  const address = ethCrypto.publicKey.toAddress(publicKey)

  console.log(`Address: ${address}`)
  console.log(`Public key: ${publicKey}`)
}

getPublicKey();