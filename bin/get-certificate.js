require("dotenv/config");
const minespider = require("../dist/index");
const ethers = require("ethers");

const sdk = new minespider.Minespider(
  String(process.env.MNSPDR_FILE_STORAGE_HOST),
  String(process.env.MNSPDR_NETWORK_HOST),
  String(process.env.MNSPDR_BLOCKCHAIN_ROOT_ACCOUNT_ADDRESS)
);

const mnemonic = process.env.MNSPDR_BLOCKCHAIN_ROOT_ACCOUNT_MNEMONIC;
const wallet = ethers.Wallet.fromMnemonic(mnemonic);

async function run() {
  certificateId = process.argv[2];

  const certificate = await sdk.getCertificateById(certificateId);
  console.log("Certificate:", certificate);

  const publicLayer = await sdk.getPublicLayerByCertificateId(certificateId);
  console.log(`Public Layer (${certificate.layers.public}):`);
  console.log(JSON.stringify(publicLayer, null, 2));

  const keys =
    publicLayer.owner.id.toLowerCase() === wallet.address.toLowerCase()
      ? publicLayer.owner.keys
      : publicLayer.recipient.keys;

  const keysLayer = await sdk.getKeysLayer(keys, wallet.privateKey);
  console.log(`\nKeys Layer (${keys}):`);
  console.log(JSON.stringify(keysLayer, null, 2));

  const recipientLayer = await sdk.getTransparencyLayer(
    publicLayer.layers.recipient,
    keysLayer.recipient.encryption_key
  );
  console.log(`\nRecipient Layer (${certificate.layers.recipient}):`);
  console.log(JSON.stringify(recipientLayer, null, 2));

  const transparencyLayer = await sdk.getTransparencyLayer(
    publicLayer.layers.transparency,
    keysLayer.transparency.encryption_key
  );
  console.log(`\nTransparency Layer (${certificate.layers.transparency}):`);
  console.log(JSON.stringify(transparencyLayer, null, 2));

  const privateLayer = await sdk.getTransparencyLayer(
    publicLayer.layers.private,
    keysLayer.private.encryption_key
  );
  console.log(`\nPrivate Layer (${certificate.layers.private}):`);
  console.log(JSON.stringify(privateLayer, null, 2));

  const links = await sdk.getLinksLayer(
    publicLayer.layers.links,
    keysLayer.links.encryption_key
  );

  console.log(`\nLinks (${publicLayer.layers.links}):`);
  console.log(JSON.stringify(links, null, 2));

  // await Promise.all(
  //   links.links.map(async (link) => {
  //     const layer = await sdk.getPublicLayerByCertificateId(link);
  //     console.log(JSON.stringify(layer, null, 2));
  //   })
  // );
}

run();
