import * as aesjs from "aes-js";
import * as crypto from "crypto";
import * as EthCrypto from "eth-crypto";
import * as keccak256 from "keccak256";

import { DomainService } from "../core/DomainService";

const stringify = require("fast-json-stable-stringify");

export type Bytes32 = string;
export interface EncryptionServiceInterface extends DomainService {
  hash(data: Buffer | string | object | Array<number>): Bytes32;
  generateRandomBytes(size: number): Buffer;
  encryptWithPassword(payload: Buffer, password: string): Promise<Buffer>;
  decryptWithPassword(payload: Buffer, password: string): Promise<Buffer>;
  encryptWithPublicKey(payload: Buffer, publicKey: Buffer): Promise<Buffer>;
  decryptWithPrivateKey(payload: Buffer, privateKey: Buffer): Promise<Buffer>;
}

class AESKey {
  constructor(readonly key: Buffer) {
    if (key.byteLength !== 32) {
      throw new Error("The key must have 32 bytes");
    }
  }
}

export type PublicKey = Buffer;
export type PrivateKey = Buffer;

export class EncryptionService implements EncryptionServiceInterface {
  public static RANDOM_BYTES_DEFAULT_SIZE = 16;

  private static generateAESKeyFromString(password: string): AESKey {
    var hash = crypto.createHash("md5");
    hash.update(password);

    return new AESKey(Buffer.from(hash.digest("hex")));
  }

  async encryptWithPassword(
    payload: Buffer,
    password: string
  ): Promise<Buffer> {
    var AESKey: AESKey = EncryptionService.generateAESKeyFromString(password);
    var aesCtr = new aesjs.ModeOfOperation.ctr(
      [...AESKey.key],
      new aesjs.Counter(5)
    );

    const raw = Buffer.from(aesCtr.encrypt(payload));

    return Buffer.from(raw.toString("hex"));
  }

  async decryptWithPassword(
    payload: Buffer,
    password: string
  ): Promise<Buffer> {
    var AESKey: AESKey = EncryptionService.generateAESKeyFromString(password);
    var aesCtr = new aesjs.ModeOfOperation.ctr(
      [...AESKey.key],
      new aesjs.Counter(5)
    );

    const encrypted = Buffer.from(payload.toString(), "hex");

    return Buffer.from(aesCtr.decrypt(encrypted));
  }

  async encryptWithPublicKey(
    payload: Buffer,
    publicKey: PublicKey
  ): Promise<Buffer> {
    const encrypted = await EthCrypto.encryptWithPublicKey(
      publicKey.toString().replace('0x', ''),
      payload.toString()
    );

    return Buffer.from(EthCrypto.cipher.stringify(encrypted));
  }

  async decryptWithPrivateKey(
    payload: Buffer,
    privateKey: PrivateKey
  ): Promise<Buffer> {
    const decrypted = await EthCrypto.decryptWithPrivateKey(
      privateKey.toString().replace(/^0x/, ''),
      EthCrypto.cipher.parse(payload.toString())
    );

    return Buffer.from(decrypted);
  }

  generateRandomBytes(
    size: number = EncryptionService.RANDOM_BYTES_DEFAULT_SIZE
  ): Buffer {
    return crypto.randomBytes(size);
  }

  hash(data: Buffer | string | object | Array<number>): Bytes32 {
    if (
      typeof data == "string" ||
      Buffer.isBuffer(data) ||
      Array.isArray(data)
    ) {
      return `0x${keccak256(data).toString("hex")}`;
    }

    return `0x${keccak256(stringify(data)).toString("hex")}`;
  }
}
