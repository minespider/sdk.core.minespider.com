export type CertificateOwnerPrivateKey = Buffer;
export type CertificateOwnerPublicKey = Buffer;
export type CertificateOwner = string;
export type CertificateCode = string;
export type CertificateVersion = number;
export type CertificateOwnerId = string;
export type CertificateOwnerName = string;
export type CertificateOwnerType = number;
