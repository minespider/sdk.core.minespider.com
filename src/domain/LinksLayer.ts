export const UNDISCLOSED = "undisclosed";

export interface CertificateLink {
  id: string | "undisclosed";
  undisclosed: boolean;
}

export interface LinksLayerDTO {
  links: string[];
  hash: string;
}

export class LinksLayer {
  readonly props: LinksLayerDTO;

  constructor(props: LinksLayerDTO) {
    this.props = props;
  }

  toDTO(): LinksLayerDTO {
    return {
      ...this.props,
    };
  }
}
