import { RecipientId, RecipientPublicKey } from "./Recipient";

export interface RecipientLayerDTO {
  id: string;
  publicKey: string;
}

export class RecipientLayer {
  constructor(
    readonly id: RecipientId,
    readonly publicKey: RecipientPublicKey
  ) {}

  toDTO(): RecipientLayerDTO {
    return {
      id: this.id,
      publicKey: this.publicKey.toString(),
    };
  }
}
