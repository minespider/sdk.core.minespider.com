import { EncryptionServiceInterface } from "../domain/EncryptionService";
import { Bus } from "../application/Bus";
import { PublicLayer } from "../domain/PublicLayer";
import { GetCertificateKeysLayerQuery } from "../application/query/GetCertificateKeysLayerQuery";
import { KeysLayer } from "../domain/KeysLayer";
import { GetFileQuery } from "../application/query/GetFileQuery";
import { EthereumAccount } from "../core/EthereumAccount";
import { TransparencyLayerDTO } from "./TransparencyLayer";
import { GetCertificateByIdQuery } from "../application/query/GetCertificateByIdQuery";
import { CertificateId } from "./CertificateId";
import { UniqueDomainEntityID } from "../core/UniqueDomainEntityID";
import { Certificate } from "./Certificate";
import { CertificateLink } from "./LinksLayer";

export class TransparencyLayerFactory {
  constructor(
    private encryptionService: EncryptionServiceInterface,
    private bus: Bus
  ) {}

  async getTransparencyLayersFromLinks(
    links: CertificateLink[],
    account: EthereumAccount
  ): Promise<TransparencyLayerDTO[]> {
    return await Promise.all(
      links.map(async (link) => {
        const certificateResult = await this.bus.execute<Certificate>(
          new GetCertificateByIdQuery(
            CertificateId.create(new UniqueDomainEntityID(link.id)).unwrap()
          )
        );
        const certificate = certificateResult.unwrap();
        const publicLayer = certificate.props.publicLayer;

        const keysLayerResult = await this.bus.execute<KeysLayer>(
          this.prepareGetCertificateKeysLayerQuery(publicLayer, account)
        );
        const fileResult = await this.bus.execute<Buffer>(
          new GetFileQuery(publicLayer.transparencyLayerAddress)
        );

        const decrypted = await this.encryptionService.decryptWithPassword(
          fileResult.unwrap(),
          keysLayerResult.unwrap().transparencyEncryptionKey
        );

        const data: TransparencyLayerDTO = JSON.parse(decrypted.toString());

        return {
          ...data,
          id: link.undisclosed === true ? data.id : null,
        };
      })
    );
  }

  private prepareGetCertificateKeysLayerQuery(
    publicLayer: PublicLayer,
    ownerAccount: EthereumAccount
  ) {
    if (publicLayer.owner.id === ownerAccount.props.address.toString()) {
      return new GetCertificateKeysLayerQuery(
        publicLayer.owner.keys,
        ownerAccount.privateKey
      );
    }

    if (!publicLayer.recipient) {
      throw new Error(
        "The recipient is required when the owner is not the author of the transaction"
      );
    }

    return new GetCertificateKeysLayerQuery(
      publicLayer.recipient.keys,
      ownerAccount.privateKey
    );
  }
}
