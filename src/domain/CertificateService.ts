import { Certificate, GrantAccessToCertificateProps } from "./Certificate";
import { Result } from "../core/Result";
import {
  CreateCertificateProps,
  EditCertificateProps,
  ConfirmCertificateProps,
  CertifyCertificateProps,
} from "./Certificate";
import { CERTIFICATE_TYPES } from "./PublicLayer";
import { EthereumAccount } from "../core/EthereumAccount";
import { CertificateId } from "./CertificateId";

export interface CreateCertificateResponse {
  certificate: Certificate;
  transaction: {
    transactionHash: string;
    blockHash: string;
  };
}

export interface EditCertificateResponse {
  certificate: Certificate;
  transaction: {
    transactionHash: string;
    blockHash: string;
  };
}

export interface CertificateServiceInterface {
  createCertificate(
    props: CreateCertificateProps,
    type: CERTIFICATE_TYPES
  ): Promise<Result<CreateCertificateResponse>>;
  getCertificate(certificateId: CertificateId): Promise<Result<Certificate>>;
  getCertificatesStorageAddress(): Promise<Result<string>>;
  editCertificate(
    props: EditCertificateProps,
    ownerAccount: EthereumAccount
  ): Promise<Result<EditCertificateResponse>>;
  confirmCertificate(
    props: ConfirmCertificateProps
  ): Promise<Result<[string, string, Certificate]>>;
  certifyCertificate(
    props: CertifyCertificateProps
  ): Promise<Result<Certificate>>;
  grantAccessToKeysLayer(
    keysLayerAddress: string,
    ownerAccount: EthereumAccount,
    newOwnerPublicKey: string
  ): Promise<Result<string>>;
  grantAccessToCertificate(
    props: GrantAccessToCertificateProps,
    ownerAccount: EthereumAccount
  ): Promise<Result<Certificate>>;
}
