export interface FileReference {
  checksum: string;
  name: string;
  type: string;
}

export interface InMemoryFile extends FileReference {
  content: Buffer;
}

export interface LocalFileReference extends FileReference {
  path: string;
}
