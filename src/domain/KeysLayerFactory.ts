import { EncryptionServiceInterface } from "./EncryptionService";
import { KeysLayer } from "./KeysLayer";

export class KeysLayerFactory {
  constructor(private encryptionService: EncryptionServiceInterface) {}

  async create() {
    return new KeysLayer(
      this.encryptionService.hash(
        this.encryptionService.generateRandomBytes(32)
      ),
      this.encryptionService.hash(
        this.encryptionService.generateRandomBytes(32)
      ),
      this.encryptionService.hash(
        this.encryptionService.generateRandomBytes(32)
      ),
      this.encryptionService.hash(
        this.encryptionService.generateRandomBytes(32)
      )
    );
  }
}
