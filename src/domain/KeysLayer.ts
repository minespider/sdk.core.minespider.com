export type SymetricEncryptionKey = string;

export interface KeysLayerDTO {
  recipient: {
    encryption_key: SymetricEncryptionKey;
  };
  private: {
    encryption_key: SymetricEncryptionKey;
  };
  transparency: {
    encryption_key: SymetricEncryptionKey;
  };
  links: {
    encryption_key: SymetricEncryptionKey;
  };
}

export class KeysLayer {
  readonly recipientEncryptionKey: SymetricEncryptionKey;
  readonly privateEncryptionKey: SymetricEncryptionKey;
  readonly transparencyEncryptionKey: SymetricEncryptionKey;
  readonly linksKey: SymetricEncryptionKey;

  constructor(
    recipientEncryptionKey: SymetricEncryptionKey,
    privateEncryptionKey: SymetricEncryptionKey,
    transparencyEncryptionKey: SymetricEncryptionKey,
    linksKey: SymetricEncryptionKey
  ) {
    this.recipientEncryptionKey = recipientEncryptionKey;
    this.privateEncryptionKey = privateEncryptionKey;
    this.transparencyEncryptionKey = transparencyEncryptionKey;
    this.linksKey = linksKey;
  }

  toDTO(): KeysLayerDTO {
    return {
      recipient: {
        encryption_key: this.recipientEncryptionKey,
      },
      private: {
        encryption_key: this.privateEncryptionKey,
      },
      transparency: {
        encryption_key: this.transparencyEncryptionKey,
      },
      links: {
        encryption_key: this.linksKey,
      },
    };
  }
}
