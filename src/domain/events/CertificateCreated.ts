import { DomainEventInterface } from "../../core/events/DomainEventInterface";
import { UniqueDomainEntityID } from "../../core/UniqueDomainEntityID";
import { Certificate } from "../Certificate";
import { CertificateId } from "../CertificateId";

export class CertificateCreated implements DomainEventInterface {
  public static EVENT_NAME: string = "CertificateCreated";
  public dateTimeOccurred: Date;
  public certificateId: CertificateId;

  constructor(certificate: Certificate) {
    this.dateTimeOccurred = new Date();
    this.certificateId = certificate.certificateId;
  }

  getId(): UniqueDomainEntityID {
    return this.certificateId.id;
  }

  getEventName(): string {
    return CertificateCreated.EVENT_NAME;
  }
}
