import { CertificateId } from "./CertificateId";
import { PublicLayer, PublicLayerMetadata } from "./PublicLayer";
import { AggregateRoot } from "../core/AggregateRoot";
import { UniqueDomainEntityID } from "../core/UniqueDomainEntityID";
import { CertificateCreated } from "./events/CertificateCreated";
import { Guard } from "../core/Guard";
import { Result } from "../core/Result";
import { RecipientId, RecipientPublicKey } from "./Recipient";
import {
  CertificateCode,
  CertificateOwnerId,
  CertificateOwnerPrivateKey,
} from "./CertificateOwner";
import { TransparencyLayerMetadata } from "./TransparencyLayer";
import { FileReference, InMemoryFile } from "./File";
import { PrivateLayerMetadata } from "./PrivateLayer";
import { KeysLayer } from "./KeysLayer";
import { EthereumAccount } from "../core/EthereumAccount";
import { CertificateLink } from "./LinksLayer";

export interface CreateCertificateProps {
  public: {
    id: CertificateId;
    parent: string | null;
    version: number;
    code: string;
    metadata: PublicLayerMetadata;
    materials: {
      type: string;
      taxonomy: string;
      amount_type: string;
      amount: number;
    }[];
    files: {
      [key: string]:
        | InMemoryFile
        | FileReference
        | Promise<InMemoryFile | FileReference>;
    };
  };
  recipient?: {
    id: RecipientId;
    publicKey: RecipientPublicKey;
  };
  owner: {
    id: CertificateOwnerId;
    privateKey: CertificateOwnerPrivateKey;
  };
  transparency: {
    metadata: TransparencyLayerMetadata;
    files: {
      [key: string]:
        | InMemoryFile
        | FileReference
        | Promise<InMemoryFile | FileReference>;
    };
  };
  private: {
    metadata: PrivateLayerMetadata;
    files: {
      [key: string]:
        | InMemoryFile
        | FileReference
        | Promise<InMemoryFile | FileReference>;
    };
  };
  links: CertificateLink[];
}

export interface EditCertificateProps {
  parentPublicLayer: string;
  parentKeysLayerHash: string;
  public: {
    id: CertificateId;
    code: string;
    version: number;
    metadata: PublicLayerMetadata;
    materials: {
      type: string;
      taxonomy: string;
      amount_type: string;
      amount: number;
    }[];
    files: {
      [key: string]:
        | InMemoryFile
        | FileReference
        | Promise<InMemoryFile | FileReference>;
    };
  };
  transparency: {
    metadata: TransparencyLayerMetadata;
    files: {
      [key: string]:
        | InMemoryFile
        | FileReference
        | Promise<InMemoryFile | FileReference>;
    };
  };
  private: {
    metadata: PrivateLayerMetadata;
    files: {
      [key: string]:
        | InMemoryFile
        | FileReference
        | Promise<InMemoryFile | FileReference>;
    };
  };
  links: CertificateLink[];
}

export interface ConfirmCertificateProps {
  parentPublicLayer: string;
  keysLayerAddress: string;
  newCertificateId: CertificateId;
  newCertificateCode: CertificateCode;
  confirmedByAccount: EthereumAccount;
}

export interface ConfirmedCertificateDTO {
  keysLayerAddress: string;
  certificate: CertificateDTO;
}

export interface CertifyCertificateProps {
  parentPublicLayer: string;
  parentKeysLayerHash: string;
  newCertificateId: CertificateId;
  newCertificateCode: CertificateCode;
  certifierAccount: EthereumAccount;
}

export interface GrantAccessToCertificateProps {
  parentPublicLayer: string;
  parentKeysLayerHash: string;
  grantAccessTo: {
    id: string;
    publicKey: string;
  };
  newCertificateId: CertificateId;
  newCertificateCode: CertificateCode;
  keysLayer: KeysLayer;
}

export interface CertificateDTO {
  id: string;
  layers: {
    public: string;
    recipient: string;
    transparency: string;
    private: string;
  };
}

interface CertificateProps {
  id: CertificateId;
  publicLayer: PublicLayer;
  publicLayerHash: string;
}

export class Certificate extends AggregateRoot<CertificateProps> {
  get certificateId(): CertificateId {
    const id = CertificateId.create(this._id);

    return id.getValue();
  }

  private constructor(props: CertificateProps, id?: UniqueDomainEntityID) {
    super(props, id ? id : props.id.id);
  }

  static create(props: {
    id: CertificateId;
    publicLayer: PublicLayer;
    publicLayerHash: string;
  }): Result<Certificate> {
    const certificateResult = Certificate.reconstitute({
      id: props.id,
      publicLayer: props.publicLayer,
      publicLayerHash: props.publicLayerHash,
    });

    if (certificateResult.isSuccess) {
      const certificate = certificateResult.unwrap();
      certificate.raise(new CertificateCreated(certificate));
    }

    return certificateResult;
  }

  toDTO(): CertificateDTO {
    return {
      id: this.id.toString(),
      layers: {
        public: this.props.publicLayerHash,
        recipient: this.props.publicLayer.recipientLayerAddress,
        transparency: this.props.publicLayer.transparencyLayerAddress,
        private: this.props.publicLayer.privateLayerAddress,
      },
    };
  }

  static reconstitute(props: CertificateProps) {
    const guardResult = Guard.againstNullOrUndefinedBulk([
      { argument: props.id, argumentName: "id" },
      { argument: props.publicLayer, argumentName: "publicLayer" },
      { argument: props.publicLayerHash, argumentName: "publicLayerHash" },
    ]);

    if (guardResult.isFailure) {
      return Result.fail<Certificate>(guardResult.error);
    }

    const certificate = new Certificate({
      id: props.id,
      publicLayer: props.publicLayer,
      publicLayerHash: props.publicLayerHash,
    });

    return Result.ok<Certificate>(certificate);
  }
}
