import { DomainEntity } from "../core/DomainEntity";
import { UniqueDomainEntityID } from "../core/UniqueDomainEntityID";
import { Result } from "../core/Result";

export class RecipientId extends DomainEntity<any> {
  get id(): UniqueDomainEntityID {
    return this._id;
  }

  private constructor(id?: UniqueDomainEntityID) {
    super(null, id);
  }

  public static create(id?: UniqueDomainEntityID): Result<RecipientId> {
    return Result.ok<RecipientId>(new RecipientId(id));
  }
}
