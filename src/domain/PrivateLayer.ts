import { FileReference } from "./File";

export interface PrivateLayerMetadata {
  [key: string]: string | number | boolean;
}

export interface PrivateLayerFiles {
  [key: string]: FileReference;
}

export interface PrivateLayerConfirmationEntity {
  id: string;
  publicKey: string;
  keys: string;
  timestamp: number;
}

export interface PrivateLayerDTO {
  metadata: PrivateLayerMetadata;
  files: PrivateLayerFiles;
}

interface PrivateLayerProps {
  metadata: PrivateLayerMetadata;
  files: PrivateLayerFiles;
}

export class PrivateLayer {
  readonly props: PrivateLayerProps;

  constructor(props: PrivateLayerProps) {
    this.props = props;
  }

  toDTO(): PrivateLayerDTO {
    return {
      ...this.props,
    };
  }
}
