import { FileReference } from "./File";

export interface TransparencyLayerMetadata {
  [key: string]: string | number | boolean;
}

export interface TransparencyLayerFiles {
  [key: string]: FileReference;
}

export interface TransparencyLayerDTO {
  id: string | null;
  metadata: TransparencyLayerMetadata;
  files: TransparencyLayerFiles;
  upstream: TransparencyLayerDTO[];
}

export class TransparencyLayer {
  readonly props: TransparencyLayerDTO;

  constructor(props: TransparencyLayerDTO) {
    this.props = props;
  }

  toDTO(): TransparencyLayerDTO {
    return {
      ...this.props,
    };
  }
}
