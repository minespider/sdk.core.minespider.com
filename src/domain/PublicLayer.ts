import { CertificateId } from "./CertificateId";
import {
  CertificateCode,
  CertificateOwner,
  CertificateOwnerPublicKey,
  CertificateVersion
} from "./CertificateOwner";
import { FileReference } from "./File";

export type RecipientLayerAddress = string;
export type TransparencyLayerAddress = string;
export type PrivateLayerAddress = string;
export type KeysLayerAddress = string;
export type LinksLayerAddress = string;

export interface PublicLayerMetadata {
  [key: string]: string | number | boolean;
}

export interface PublicLayerMaterial {
  type: string;
  taxonomy: string;
  amount_type: string;
  amount: number;
}

export enum CERTIFICATE_TYPES {
  //@deprecated
  SHIPMENT = "SHIPMENT",
  PRODUCT_PASSPORT = "PRODUCT_PASSPORT",
  ENTITY = "ENTITY",
}

export enum CERTIFICATE_ACTIONS {
  CREATED = "CREATED",
  CONFIRMED = "CONFIRMED",
  EDITED = "EDITED",
  VALIDATED = "VALIDATED",
  CERTIFIED = "CERTIFIED",
}

export interface PublicLayerCertifier {
  id: string;
  publicKey: string;
  keys: string;
  timestamp: number;
}

export interface PublicLayerFiles {
  [key: string]: FileReference;
}

export interface PublicLayerDTO {
  id: string;
  code: string;
  version: number;
  type: string;
  action: string;
  parent: string | null;
  owner: {
    id: string;
    publicKey: string;
    keys: string;
  };
  recipient?: {
    keys: string;
  };
  materials: PublicLayerMaterial[];
  metadata: PublicLayerMetadata;
  layers: {
    recipient: string;
    transparency: string;
    private: string;
    links: string;
  };
  creation_timestamp: number;
  files: PublicLayerFiles;
}

interface PublicLayerProps {
  id: CertificateId;
  code: CertificateCode;
  version: CertificateVersion;
  parent: CertificateId | null;
  owner: {
    id: CertificateOwner;
    publicKey: CertificateOwnerPublicKey;
    keys: KeysLayerAddress;
  };
  recipient?: {
    keys: KeysLayerAddress;
  };
  type: CERTIFICATE_TYPES;
  action: CERTIFICATE_ACTIONS;
  metadata: PublicLayerMetadata;
  materials: PublicLayerMaterial[];
  recipientLayerAddress: RecipientLayerAddress;
  privateLayerAddress: PrivateLayerAddress;
  transparencyLayerAddress: TransparencyLayerAddress;
  linksLayerAddress: LinksLayerAddress;
  creationTimestamp: number;
  files: PublicLayerFiles;
}

export class PublicLayer {
  readonly id: CertificateId;
  readonly code: CertificateCode;
  readonly version: CertificateVersion;
  readonly parent: CertificateId | null;
  readonly owner: {
    id: CertificateOwner;
    publicKey: CertificateOwnerPublicKey;
    keys: string;
  };
  readonly recipient?: {
    keys: string;
  };
  readonly type: CERTIFICATE_TYPES;
  readonly action: CERTIFICATE_ACTIONS;
  readonly metadata: PublicLayerMetadata;
  readonly materials: PublicLayerMaterial[];
  readonly recipientLayerAddress: string;
  readonly privateLayerAddress: string;
  readonly transparencyLayerAddress: string;
  readonly linksLayerAddress: string;
  readonly creationTimestamp: number;
  readonly files: PublicLayerFiles;

  constructor(props: PublicLayerProps) {
    this.id = props.id;
    this.code = props.code;
    this.version = props.version;
    this.parent = props.parent;
    this.materials = props.materials;
    this.owner = props.owner;
    this.recipient = props.recipient;
    this.action = props.action;
    this.type = props.type;
    this.metadata = props.metadata;
    this.privateLayerAddress = props.privateLayerAddress;
    this.recipientLayerAddress = props.recipientLayerAddress;
    this.transparencyLayerAddress = props.transparencyLayerAddress;
    this.linksLayerAddress = props.linksLayerAddress;
    this.creationTimestamp = props.creationTimestamp;
    this.files = props.files;
  }

  toDTO(): PublicLayerDTO {
    return {
      id: this.id.id.toString(),
      code: this.code,
      version: this.version,
      parent: this.parent ? this.parent.id.toString() : null,
      owner: {
        id: this.owner.id,
        keys: this.owner.keys,
        publicKey: this.owner.publicKey.toString()
      },
      recipient: this.recipient,
      type: this.type,
      action: this.action,
      metadata: this.metadata,
      materials: this.materials,
      layers: {
        recipient: this.recipientLayerAddress,
        transparency: this.transparencyLayerAddress,
        private: this.privateLayerAddress,
        links: this.linksLayerAddress
      },
      creation_timestamp: this.creationTimestamp,
      files: this.files
    };
  }
}
