import { EncryptionServiceInterface } from "./EncryptionService";
import { Result } from "../core/Result";
import { CertificateLink, LinksLayer } from "./LinksLayer";

export class LinksLayerFactory {
  constructor(private encryptionService: EncryptionServiceInterface) {}

  async create(links: CertificateLink[]): Promise<Result<LinksLayer>> {
    const linkIds: string[] = links.map((link) => link.id);

    const linksLayer = new LinksLayer({
      links: linkIds,
      hash: this.encryptionService.hash(linkIds),
    });

    return Result.ok(linksLayer);
  }
}
