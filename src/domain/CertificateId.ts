import { DomainEntity } from "../core/DomainEntity";
import { UniqueDomainEntityID } from "../core/UniqueDomainEntityID";
import { Result } from "../core/Result";

export class CertificateId extends DomainEntity<any> {
  get id(): UniqueDomainEntityID {
    return this._id;
  }

  toString(): string {
    return this._id.toString();
  }

  private constructor(id?: UniqueDomainEntityID) {
    super(null, id);
  }

  public static create(id?: UniqueDomainEntityID): Result<CertificateId> {
    return Result.ok<CertificateId>(new CertificateId(id));
  }
}
