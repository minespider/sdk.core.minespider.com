import * as ethers from "ethers";
import { BigNumber } from "ethers";
import {
  BlockchainCertificate,
  BlockchainServiceInterface
} from "../../application/BlockchainService";
import { Result } from "../../core/Result";
import certificatesABI from "./contracts/Certificates";
import { EntityService } from "./EntityService";

interface TransactionResponse {
  transactionHash: string;
  blockHash: string;
}

export class BlockchainService implements BlockchainServiceInterface {
  private readonly contract: ethers.Contract;
  private readonly wallet: ethers.Wallet;
  private readonly entityService: EntityService;

  constructor(
    private blockchainHost: string,
    private certificatesContractAddress: string
  ) {
    this.entityService = new EntityService(blockchainHost, certificatesContractAddress);
    this.wallet = ethers.Wallet.createRandom().connect(
      new ethers.providers.JsonRpcProvider(this.blockchainHost)
    );

    this.contract = new ethers.Contract(
      this.certificatesContractAddress,
      certificatesABI,
      this.wallet
    );
  }

  async createCertificate(
    privateKey: string,
    certificate: {
      id: string;
      code: string;
      version: number;
      parent: string;
      creationTimestamp: number;
      publicLayerHash: string;
      certificateHash: string;
    }
  ): Promise<Result<TransactionResponse>> {
    const { contract } = this.entityService.resolve(privateKey);

    let response: TransactionResponse | undefined;
    let shouldRetry = false;
    let error: Error | undefined;

    for (let retries = 0; retries < 3; retries++) {
      try {
        const estimatedGas = await contract.estimateGas.createCertificate(
          certificate.id,
          certificate.code,
          certificate.version,
          certificate.parent,
          certificate.creationTimestamp,
          certificate.publicLayerHash,
          certificate.certificateHash
        );

        const tx = await contract.createCertificate(
          certificate.id,
          certificate.code,
          certificate.version,
          certificate.parent,
          certificate.creationTimestamp,
          certificate.publicLayerHash,
          certificate.certificateHash,
          {
            gasLimit: estimatedGas.toHexString()
          }
        );

        const receipt: ethers.ContractReceipt = await tx.wait();

        response = {
          transactionHash: receipt.transactionHash,
          blockHash: receipt.blockHash
        };
      } catch (e: any) {
        error = e;
        if (
          typeof e.message === "string" &&
          e.message.includes("nonce has already been used")
        ) {
          shouldRetry = true;
        }
      }

      if (response || !shouldRetry) {
        break;
      }
    }

    if (!response) {
      return Result.fail(
        new Error(`Error while creating certificate ${certificate.id}: ${error}`)
      );
    }

    return Result.ok(response);
  }

  async getCertificate(
    certificateId: string
  ): Promise<Result<BlockchainCertificate>> {
    try {
      const certificate = await this.contract.getCertificate(
        certificateId.toString()
      );

      if (
        certificate["public_layer"] ===
        "0x0000000000000000000000000000000000000000000000000000000000000000"
      ) {
        return Result.fail(
          new Error(`Certificate ${certificateId} not found on the Blockchain`)
        );
      }

      return Result.ok({
        id: certificate["id"],
        code: certificate["code"],
        version: certificate["version"],
        parent: certificate["parent"] != "" ? certificate["parent"] : null,
        creationTimestamp: (
          certificate["creation_timestamp"] as BigNumber
        ).toNumber(),
        publicLayerHash: certificate["public_layer"],
        certificateHash: certificate["certificateHash"]
      });
    } catch (e: any) {
      console.error(`Unable to get certificate with id ${certificateId}. Because ${e.message}.`);
      throw e;
    }
  }

  async getCertificatesStorageAddress(): Promise<Result<string>> {
    try {
      return Result.ok(await this.contract.getCertificatesStorageAddress());
    } catch (e) {
      console.error(e);
      throw e;
    }
  }
}
