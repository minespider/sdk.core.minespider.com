import * as ethers from "ethers";
import certificatesABI from "./contracts/Certificates";

export class EntityService {
  constructor(private blockchainHost: string, private certificatesContractAddress: string) {

  }

  resolve(privateKey: string) {
    let wallet;
    let contract;
    try {
      wallet = new ethers.Wallet(
        privateKey,
        new ethers.providers.JsonRpcProvider(this.blockchainHost)
      );
    } catch (e: any) {
      console.error(`Can't get the wallet because ${e.message}`);
      throw e;
    }
    try {
      contract = new ethers.Contract(
        this.certificatesContractAddress,
        certificatesABI,
        wallet
      );
    } catch (e: any) {
      console.error(`Can't get the contract because ${e.message}`);
      throw e;
    }

    return { wallet, contract };
  }
}