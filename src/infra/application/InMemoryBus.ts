import { Bus, Request } from "../../application/Bus";
import { Result } from "../../core/Result";
import { Guard } from "../../core/Guard";
import { UseCase } from "../../core/UseCase";

export class InMemoryBus implements Bus {
  private useCases: Map<string, UseCase<any, any> | Function> = new Map();

  addUseCase<Request, Response>(
    name: string,
    useCase: UseCase<Request, Response> | Function
  ): Result<Response> {
    const guardResult = Guard.againstNullOrUndefinedBulk([
      { argument: name, argumentName: "name" },
      { argument: useCase, argumentName: "useCase" },
    ]);

    if (guardResult.isFailure) {
      return Result.fail<Response>(guardResult.error);
    }

    if (this.useCases.has(name)) {
      return Result.fail<Response>(
        new Error(`There is already a Use Case assigned to ${name}`)
      );
    }

    this.useCases.set(name, useCase);

    return Result.ok<Response>();
  }

  async execute<Response>(request: Request): Promise<Result<Response>> {
    const useCase = this.useCases.get(request.getName());

    if (!useCase) {
      return Result.fail<Response>(
        new Error(`No Use Case not assigned for ${request.getName()}`)
      );
    }

    if (typeof useCase == "function") {
      return useCase(request);
    }

    return useCase.execute(request);
  }
}
