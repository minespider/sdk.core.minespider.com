import { FileStorageServiceInterface } from "../application/FileStorageService";
import { Result } from "../core/Result";
import { InMemoryFile } from "../domain/File";
import axios from "axios";
import fetch from "node-fetch";

const FormData = require("form-data");

export class FileStorageService implements FileStorageServiceInterface {
  constructor(private fileStorageHost: string) {
  }

  async upload(file: InMemoryFile): Promise<Result<string>> {
    try {
      const formData = new FormData();
      formData.append("file", file.content, file.name);

      const response = await fetch(this.fileStorageHost + "/upload", {
        method: "POST",
        body: formData,
        headers: formData.getHeaders()
      });
      const data = await response.json() as any;
      const address = data?.address || data?.content;
      if (response.status < 200 || response.status >= 300) {
        const message = `Unable to upload file with name ${file.name} :\nRequest failed with status code ${response.status}.`;
        console.error(message);
        return Result.fail(new Error(message));
      }
      if (!address) {
        const message = `Unable to upload file with name ${file.name}:\nNo address in response from File Storage.`;
        console.error(message);
        return Result.fail(new Error(message));
      }

      return Result.ok(address);
    } catch (error: any) {
      const message = `Unable to upload file with name ${file.name}:\n${error.message}`;
      console.error(message);
      return Result.fail(new Error(message));
    }
  }

  async download(hash: string): Promise<Result<Buffer>> {
    try {
      let response = await axios.get(
        this.fileStorageHost + `/download?filename=${hash}`
      );

      if (!response.data) {
        const message = `Unable to download data for hash ${hash}`;
        console.error(message);
        return Result.fail(new Error(message));
      }

      let data =
        typeof response.data === "object"
          ? JSON.stringify(response.data)
          : response.data;

      return Result.ok(Buffer.from(data));
    } catch (e: any) {
      const message = `Unable to download data for hash ${hash}:\n${e.message}`;
      console.error(message);
      return Result.fail(new Error(message));
    }
  }
}
