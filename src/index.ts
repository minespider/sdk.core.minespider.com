import { GetCertificateKeysLayerQuery } from "./application/query/GetCertificateKeysLayerQuery";
import { GetCertificatePublicLayerQuery } from "./application/query/GetCertificatePublicLayerQuery";
import { GetCertificateRecipientLayerQuery } from "./application/query/GetCertificateRecipientLayerQuery";
import { GetFileQuery } from "./application/query/GetFileQuery";
import { InMemoryBus } from "./infra/application/InMemoryBus";
import { GetCertificateStorageAddressQuery } from "./application/query/GetCertificateStorageAddressQuery";
import { GetCertificateStorageAddressUseCase } from "./application/query/GetCertificateStorageAddressUseCase";
import { GetCertificatePublicLayerUseCase } from "./application/query/GetCertificatePublicLayerUseCase";
import { GetCertificateKeysLayerUseCase } from "./application/query/GetCertificateKeysLayerUseCase";
import { CreateProductPassportUseCase } from "./application/command/CreateProductPassportUseCase";
import { ConfirmCertificateUseCase } from "./application/command/ConfirmCertificateUseCase";
import { CreateEntityCertificateUseCase } from "./application/command/CreateEntityCertificateUseCase";
import { GetCertificateTransparencyLayerUseCase } from "./application/query/GetCertificateTransparencyLayerUseCase";
import { GetCertificateRecipientLayerUseCase } from "./application/query/GetCertificateRecipientLayerUseCase";
import { GetFileUseCase } from "./application/query/GetFileUseCase";
import { GetCertificatePrivateLayerUseCase } from "./application/query/GetCertificatePrivateLayerUseCase";
import { FileStorageService } from "./infra/FileStorageService";
import {
  EncryptionService,
  EncryptionServiceInterface,
} from "./domain/EncryptionService";
import { CertificateService } from "./application/CertificateService";
import { GetCertificateTransparencyLayerQuery } from "./application/query/GetCertificateTransparencyLayerQuery";
import { GetCertificatePrivateLayerQuery } from "./application/query/GetCertificatePrivateLayerQuery";
import {
  CreateProductPassportCommand,
  CreateProductPassportProps,
} from "./application/command/CreateProductPassportCommand";
import {
  CreateEntityCertificateCommand,
  CreateEntityCertificateProps,
} from "./application/command/CreateEntityCertificateCommand";
import { KeysLayerFactory } from "./domain/KeysLayerFactory";
import { BlockchainService } from "./infra/blockchain/BlockchainService";
import {
  Certificate,
  CertificateDTO,
  EditCertificateProps,
  ConfirmCertificateProps,
  CertifyCertificateProps,
} from "./domain/Certificate";
import { PublicLayer, PublicLayerDTO } from "./domain/PublicLayer";
import { PrivateLayer, PrivateLayerDTO } from "./domain/PrivateLayer";
import {
  KeysLayer,
  KeysLayerDTO,
  SymetricEncryptionKey,
} from "./domain/KeysLayer";
import {
  TransparencyLayer,
  TransparencyLayerDTO,
} from "./domain/TransparencyLayer";
import { RecipientLayer, RecipientLayerDTO } from "./domain/RecipientLayer";
import { EthereumAccount } from "./core/EthereumAccount";
import { EditCertificateCommand } from "./application/command/EditCertificateCommand";
import { EditCertificateUseCase } from "./application/command/EditCertificateUseCase";
import { ConfirmCertificateCommand } from "./application/command/ConfirmCertificateCommand";
import { CertifyCertificateCommand } from "./application/command/CertifyCertificateCommand";
import { CertifyCertificateUseCase } from "./application/command/CertifyCertificateUseCase";
import { GetCertificateLinksLayerQuery } from "./application/query/GetCertificateLinksLayerQuery";
import { GetCertificateLinksLayerUseCase } from "./application/query/GetCertificateLinksLayerUseCase";
import { GrantAccessToCertificateLayerCommand } from "./application/command/GrantAccessToCertificateLayerCommand";
import { GrantAccessToCertificateLayerUseCase } from "./application/command/GrantAccessToCertificateLayerUseCase";
import { GetCertificatePublicLayerByCertificateIdQuery } from "./application/query/GetCertificatePublicLayerByCertificateIdQuery";
import { GetCertificatePublicLayerByCertificateIdUseCase } from "./application/query/GetCertificatePublicLayerByCertificateIdUseCase";
import { GetCertificateByIdQuery } from "./application/query/GetCertificateByIdQuery";
import { GetCertificateByIdUseCase } from "./application/query/GetCertificateByIdUseCase";
import { InMemoryFile } from "./domain/File";
import { CertificateId } from "./domain/CertificateId";
import { UniqueDomainEntityID } from "./core/UniqueDomainEntityID";
import {
  CertificateServiceInterface,
  CreateCertificateResponse,
  EditCertificateResponse
} from "./domain/CertificateService";
import { LinksLayerFactory } from "./domain/LinksLayerFactory";
import { TransparencyLayerFactory } from "./domain/TransparencyLayerFactory";
import { LinksLayer, LinksLayerDTO } from "./domain/LinksLayer";
import { BlockchainServiceInterface } from "./application/BlockchainService";

export enum LAYERS {
  PRIVATE,
  TRANSPARENCY,
  PUBLIC,
  RECIPIENT,
}

export * from "./domain/CertificateId";
export * from "./domain/File";
export * from "./core/UniqueDomainEntityID";

export class Minespider {
  private bus: InMemoryBus;
  private encryptionService: EncryptionServiceInterface;
  public  certificateService: CertificateServiceInterface;
  public blockchainService: BlockchainServiceInterface;

  constructor(
    private fileStorageServiceHost: string,
    private blockchainServiceHost: string,
    private certificatesContractAddress: string
  ) {
    this.bus = new InMemoryBus();
    this.encryptionService = new EncryptionService();
    const fileStorageService = new FileStorageService(
      this.fileStorageServiceHost
    );
    const keysLayerFactory = new KeysLayerFactory(this.encryptionService);
    const transparencyLayerFactory = new TransparencyLayerFactory(
      this.encryptionService,
      this.bus
    );
    const linksLayerFactory = new LinksLayerFactory(this.encryptionService);

    const blockchainService = new BlockchainService(
      this.blockchainServiceHost,
      this.certificatesContractAddress
    );
    this.blockchainService = blockchainService

    const certificateService = new CertificateService(
      this.encryptionService,
      keysLayerFactory,
      transparencyLayerFactory,
      linksLayerFactory,
      fileStorageService,
      blockchainService,
      this.bus
    );
    this.certificateService = certificateService

    this.bus.addUseCase(
      GetCertificatePublicLayerQuery.NAME,
      new GetCertificatePublicLayerUseCase(fileStorageService)
    );

    this.bus.addUseCase(
      GetCertificateKeysLayerQuery.NAME,
      new GetCertificateKeysLayerUseCase(
        fileStorageService,
        this.encryptionService
      )
    );

    this.bus.addUseCase(
      GetCertificateTransparencyLayerQuery.NAME,
      new GetCertificateTransparencyLayerUseCase(
        fileStorageService,
        this.encryptionService
      )
    );

    this.bus.addUseCase(
      GetCertificatePrivateLayerQuery.NAME,
      new GetCertificatePrivateLayerUseCase(
        fileStorageService,
        this.encryptionService
      )
    );

    this.bus.addUseCase(
      GetCertificateRecipientLayerQuery.NAME,
      new GetCertificateRecipientLayerUseCase(
        fileStorageService,
        this.encryptionService
      )
    );

    this.bus.addUseCase(
      GetCertificateLinksLayerQuery.NAME,
      new GetCertificateLinksLayerUseCase(
        fileStorageService,
        this.encryptionService
      )
    );

    this.bus.addUseCase(
      GetFileQuery.NAME,
      new GetFileUseCase(fileStorageService)
    );

    this.bus.addUseCase(
      GetCertificatePublicLayerByCertificateIdQuery.NAME,
      new GetCertificatePublicLayerByCertificateIdUseCase(certificateService)
    );

    this.bus.addUseCase(
      GetCertificateByIdQuery.NAME,
      new GetCertificateByIdUseCase(certificateService)
    );

    this.bus.addUseCase(
      GetCertificateStorageAddressQuery.NAME,
      new GetCertificateStorageAddressUseCase(certificateService)
    );

    this.bus.addUseCase(
      CreateProductPassportCommand.NAME,
      new CreateProductPassportUseCase(certificateService)
    );

    this.bus.addUseCase(
      CreateEntityCertificateCommand.NAME,
      new CreateEntityCertificateUseCase(certificateService)
    );

    this.bus.addUseCase(
      EditCertificateCommand.NAME,
      new EditCertificateUseCase(certificateService)
    );

    this.bus.addUseCase(
      ConfirmCertificateCommand.NAME,
      new ConfirmCertificateUseCase(certificateService)
    );

    this.bus.addUseCase(
      CertifyCertificateCommand.NAME,
      new CertifyCertificateUseCase(certificateService)
    );

    this.bus.addUseCase(
      GrantAccessToCertificateLayerCommand.NAME,
      new GrantAccessToCertificateLayerUseCase(certificateService, this.bus)
    );
  }

  async createProductPassport(
    props: CreateProductPassportProps
  ): Promise<{
    certificate: CertificateDTO;
    transaction: {
      transactionHash: string;
      blockHash: string;
    };
  }> {
    const command = new CreateProductPassportCommand(props);
    const result = await this.bus.execute<CreateCertificateResponse>(command);
    const certificate = result.unwrap().certificate;

    return {
      certificate: certificate.toDTO(),
      transaction: result.unwrap().transaction,
    };
  }

  async createEntityCertificate(props: CreateEntityCertificateProps): Promise<{
    certificate: CertificateDTO;
    transaction: {
      transactionHash: string;
      blockHash: string;
    };
  }> {
    const command = new CreateEntityCertificateCommand(props);
    const result = await this.bus.execute<CreateCertificateResponse>(command);
    const certificate = result.unwrap().certificate;

    return {
      certificate: certificate.toDTO(),
      transaction: result.unwrap().transaction,
    };
  }

  async editCertificate(
    props: EditCertificateProps,
    account: EthereumAccount
  ): Promise<{
    certificate: CertificateDTO;
    transaction: {
      transactionHash: string;
      blockHash: string;
    };
  }> {
    const command = new EditCertificateCommand(props, account);
    const result = await this.bus.execute<EditCertificateResponse>(command);
    const certificate = result.unwrap().certificate;

    return {
      certificate: certificate.toDTO(),
      transaction: result.unwrap().transaction,
    };
  }

  async confirmCertificate(
    props: ConfirmCertificateProps
  ): Promise<[string, string, CertificateDTO]> {
    const command = new ConfirmCertificateCommand(props);
    const certificatesResult = await this.bus.execute<
      [string, string, Certificate]
    >(command);
    const [
      originalOwnerKeysLayerHash,
      confirmedByKeysLayerAddress,
      certificate,
    ] = certificatesResult.unwrap();

    return [
      originalOwnerKeysLayerHash,
      confirmedByKeysLayerAddress,
      certificate.toDTO(),
    ];
  }

  async certifyCertificate(
    props: CertifyCertificateProps
  ): Promise<CertificateDTO> {
    const command = new CertifyCertificateCommand(props);
    const certificatesResult = await this.bus.execute<Certificate>(command);

    return certificatesResult.unwrap().toDTO();
  }

  async grantAccessToCertificate(
    publicLayerAddress: string,
    ownerAccount: EthereumAccount,
    grantAccessTo: {
      id: string;
      publicKey: string;
    }
  ): Promise<string> {
    const command = new GrantAccessToCertificateLayerCommand(
      publicLayerAddress,
      ownerAccount,
      grantAccessTo
    );
    const certificatesResult = await this.bus.execute<string>(command);

    return certificatesResult.unwrap();
  }

  async getPublicLayer(publicLayerHash: string): Promise<PublicLayerDTO> {
    const publicLayerResult = await this.bus.execute<PublicLayer>(
      new GetCertificatePublicLayerQuery(publicLayerHash)
    );
    const publicLayer = publicLayerResult.unwrap();

    return publicLayer.toDTO();
  }

  async getPrivateLayer(
    privateLayerHash: string,
    privateLayerEncryptionKey: string
  ): Promise<PrivateLayerDTO> {
    const privateLayerResult = await this.bus.execute<PrivateLayer>(
      new GetCertificatePrivateLayerQuery(
        privateLayerHash,
        privateLayerEncryptionKey
      )
    );
    const privateLayer = privateLayerResult.unwrap();

    return privateLayer.toDTO();
  }

  async getRecipientLayer(
    recipientLayerHash: string,
    recipientLayerEncryptionKey: string
  ): Promise<RecipientLayerDTO> {
    const recipientLayerResult = await this.bus.execute<RecipientLayer>(
      new GetCertificateRecipientLayerQuery(
        recipientLayerHash,
        recipientLayerEncryptionKey
      )
    );
    const recipientLayer = recipientLayerResult.unwrap();

    return recipientLayer.toDTO();
  }

  async getTransparencyLayer(
    transparencyLayerHash: string,
    transparencyLayerEncryptionKey: string
  ): Promise<TransparencyLayerDTO> {
    const transparencyLayerResult = await this.bus.execute<TransparencyLayer>(
      new GetCertificateTransparencyLayerQuery(
        transparencyLayerHash,
        transparencyLayerEncryptionKey
      )
    );
    const transparencyLayer = transparencyLayerResult.unwrap();

    return transparencyLayer.toDTO();
  }

  async getLinksLayer(
    linksLayerHash: string,
    linksLayerEncryptionKey: string
  ): Promise<LinksLayerDTO> {
    const linksLayerResult = await this.bus.execute<LinksLayer>(
      new GetCertificateLinksLayerQuery(
        linksLayerHash,
        linksLayerEncryptionKey
      )
    );
    const linksLayer = linksLayerResult.unwrap();

    return linksLayer.toDTO();
  }

  async getKeysLayer(
    keysLayerHash: string,
    privateKey: Buffer
  ): Promise<KeysLayerDTO> {
    const keysLayerResult = await this.bus.execute<KeysLayer>(
      new GetCertificateKeysLayerQuery(keysLayerHash, privateKey)
    );
    const keysLayer = keysLayerResult.unwrap();

    return keysLayer.toDTO();
  }

  async getPrivateFile(
    hash: string,
    encryptionKey: string,
    privateLayerHash: string
  ): Promise<InMemoryFile> {
    const fileResult = await this.bus.execute<Buffer>(new GetFileQuery(hash));
    const privateLayer = await this.getPrivateLayer(
      privateLayerHash,
      encryptionKey
    );
    const fileReference = privateLayer.files[hash];

    return {
      ...fileReference,
      content: await this.decrypt(fileResult.unwrap(), encryptionKey),
    };
  }

  async getTransparencyFile(
    hash: string,
    encryptionKey: string,
    transparencyLayerHash: string
  ): Promise<InMemoryFile> {
    const fileResult = await this.bus.execute<Buffer>(new GetFileQuery(hash));
    const transparencyLayer = await this.getTransparencyLayer(
      transparencyLayerHash,
      encryptionKey
    );
    const fileReference = transparencyLayer.files[hash];

    return {
      ...fileReference,
      content: await this.decrypt(fileResult.unwrap(), encryptionKey),
    };
  }

  async getFileContent(hash: string, encryptionKey: string): Promise<Buffer> {
    const fileResult = await this.bus.execute<Buffer>(new GetFileQuery(hash));
    const content = fileResult.unwrap();

    return this.decrypt(content, encryptionKey);
  }

  async decrypt(content: Buffer, decryptionKey: SymetricEncryptionKey) {
    return this.encryptionService.decryptWithPassword(content, decryptionKey);
  }

  async getPublicLayerByCertificateId(
    certificateId: string
  ): Promise<PublicLayerDTO> {
    const publicLayerResult = await this.bus.execute<PublicLayer>(
      new GetCertificatePublicLayerByCertificateIdQuery(
        CertificateId.create(new UniqueDomainEntityID(certificateId)).unwrap()
      )
    );
    const publicLayer = publicLayerResult.unwrap();

    return publicLayer.toDTO();
  }

  async getCertificateById(certificateId: string): Promise<CertificateDTO> {
    const certificateResult = await this.bus.execute<Certificate>(
      new GetCertificateByIdQuery(
        CertificateId.create(new UniqueDomainEntityID(certificateId)).unwrap()
      )
    );
    const certificate = certificateResult.unwrap();

    return certificate.toDTO();
  }

  async getCertificatesStorageAddress(): Promise<string> {
    const certificateResult = await this.bus.execute<string>(
      new GetCertificateStorageAddressQuery()
    );

    return certificateResult.unwrap();
  }
}
