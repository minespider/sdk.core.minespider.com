import { DomainEntity } from "./DomainEntity";
import { DomainEventInterface } from "./events/DomainEventInterface";
import { UniqueDomainEntityID } from "./UniqueDomainEntityID";

export abstract class AggregateRoot<T> extends DomainEntity<T> {
  private _domainEvents: DomainEventInterface[] = [];

  get id(): UniqueDomainEntityID {
    return this._id;
  }

  get domainEvents(): DomainEventInterface[] {
    return this._domainEvents;
  }

  protected raise(domainEvent: DomainEventInterface): void {
    this._domainEvents.push(domainEvent);
  }

  public releaseEvents(): DomainEventInterface[] {
    const events = this._domainEvents;

    this._domainEvents = [];

    return events;
  }
}
