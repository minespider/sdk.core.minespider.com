import { UniqueDomainEntityID } from "../UniqueDomainEntityID";

export interface DomainEventInterface {
  dateTimeOccurred: Date;
  getId(): UniqueDomainEntityID;
  getEventName(): string;
}
