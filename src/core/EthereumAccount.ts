import * as ethers from "ethers";

import { ValueObject } from "./ValueObject";
import { Result } from "./Result";
import { Guard } from "./Guard";

interface EthereumAccountProps {
  mnemonic?: Buffer;
  address: Buffer;
  publicKey: Buffer;
  privateKey: Buffer;
}

export class EthereumAccount extends ValueObject<EthereumAccountProps> {
  get mnemonic(): Buffer | undefined {
    return this.props.mnemonic;
  }

  get publicKey(): Buffer {
    return this.props.publicKey;
  }

  get privateKey(): Buffer {
    return this.props.privateKey;
  }

  private constructor(props: EthereumAccountProps) {
    super(props);
  }

  public static create(privateKey: string) {
    const guardResult = Guard.againstNullOrUndefined(privateKey, "privateKey");

    if (!guardResult.isSuccess) {
      return Result.fail<EthereumAccount>(guardResult.error);
    }

    try {
      const wallet = new ethers.Wallet(privateKey);

      return Result.ok<EthereumAccount>(
        new EthereumAccount({
          address: Buffer.from(wallet.address),
          privateKey: Buffer.from(wallet.privateKey),
          publicKey: Buffer.from(wallet.publicKey),
        })
      );
    } catch (error: any) {
      return Result.fail<EthereumAccount>(error.message);
    }
  }

  public static createFromMnemonic(mnemonic: string): Result<EthereumAccount> {
    const guardResult = Guard.againstNullOrUndefined(mnemonic, "mnemonic");

    if (!guardResult.isSuccess) {
      return Result.fail<EthereumAccount>(guardResult.error);
    }

    try {
      const wallet = ethers.Wallet.fromMnemonic(mnemonic);

      return Result.ok<EthereumAccount>(
        new EthereumAccount({
          mnemonic: Buffer.from(mnemonic),
          address: Buffer.from(wallet.address),
          privateKey: Buffer.from(wallet.privateKey),
          publicKey: Buffer.from(wallet.publicKey),
        })
      );
    } catch (error: any) {
      return Result.fail<EthereumAccount>(error.message);
    }
  }
}
