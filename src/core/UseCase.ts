import { Result } from "./Result";

export interface UseCase<Request, Response> {
  execute(request: Request): Promise<Result<Response>> | Response;
}
