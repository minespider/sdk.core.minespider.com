/**
 * Error raised in Result layer. This error may carry a previous error.
 */
export class ResultError extends Error {
  constructor(message: string, readonly previousError?: Error) {
    super(message);
    Object.setPrototypeOf(this, new.target.prototype);
  }
}

export class UnknownError extends Error {}

export class NotFoundException extends Error {}

export class DoesNotExist extends Error {}

export class DeletionNotAllowed extends Error {}

export class InvalidResult extends Error {}

export class ForbiddenResult extends Error {}

/**
 * Process Result, responsible to deal with any request that needs a common
 * approach to return the expected result or a known break of flow, i.e.
 * exception.
 */
export class Result<T = any> {
  public isSuccess: boolean;
  public isFailure: boolean;
  public error: Error;
  private readonly _value: T | undefined;

  protected constructor(value?: T, error?: Error) {
    this.isSuccess = error === undefined;
    this.isFailure = !this.isSuccess;

    if (this.isFailure === true && !error) {
      throw new Error("Invalid failure result. The error is required");
    }

    this.error = error || new UnknownError();
    this._value = value;

    Object.freeze(this);
  }

  /**
   * Retrieves the expected and successful value of a function's or
   * method's request.
   */
  public getValue(): T {
    if (!this.isSuccess) {
      throw new Error(
        "Can't get the value of an error result. Use 'errorValue' instead."
      );
    }

    return this._value as any;
  }

  /**
   * Unwraps the result processed after a function's or method's request.
   * If successful, the result will return as expected, otherwise a ResultError
   * will be thrown.
   *
   * @param errorMessage - Message to override if some error occurs.
   */
  public unwrap(errorMessage?: string): T {
    if (this.isFailure) {
      this.throw(errorMessage);
    }

    return this.getValue();
  }

  /**
   * Throws ResultError exception as a break of the flow, with a predictable
   * error that happens inside a function or a method.
   *
   * @param errorMessage - Message to override if some error occurs.
   */
  public throw(errorMessage?: string): ResultError {
    if (errorMessage) {
      throw new ResultError(errorMessage);
    }

    if (!this.error) {
      this.error = new Error("An unknown error happened");
    }

    throw new ResultError(this.error.message, this.error);
  }

  /**
   * Retrieves error message if result fails.
   */
  public errorValue(): string {
    return this.error.message;
  }

  /**
   * Unwraps the message of a Result instance.
   *
   * @see Result.unwrap
   * @param result - Result instance
   * @param errorMessage - Message to override if some error occurs.
   */
  public static unwrap<T>(result: Result, errorMessage?: string): T {
    return result.unwrap(errorMessage);
  }

  /**
   * Returns a successful result as expected from a function or method.
   * @param value - Value to be returned from the function or method that
   * creates the Result instance.
   */
  public static ok<T>(value?: T): Result<T> {
    return new Result<T>(value);
  }

  /**
   * Returns a failure result (exception) as expected from a function or method.
   * @param error - Error from the failure result, which can be any Error
   * instance or, as recommended, one of the known Result exceptions.
   */
  public static fail<T>(error: Error): Result<T> {
    if (typeof error === "string") {
      error = new Error(error);
    }

    return new Result<T>(undefined, error);
  }

  /**
   * Combines different result instances checking whether all of them are
   * successful. If any is not, its respective ResultError will be returned.
   * @param results - Collection of Result instance
   */
  public static combine<T>(results: Result[]): Result<T> {
    for (let result of results) {
      if (result.isFailure) return Result.fail<T>(result.error);
    }
    return Result.ok<T>();
  }
}

export type Either<L, A> = Left<L, A> | Right<L, A>;

export class Left<L, A> {
  readonly value: L;

  constructor(value: L) {
    this.value = value;
  }

  isLeft(): this is Left<L, A> {
    return true;
  }

  isRight(): this is Right<L, A> {
    return false;
  }
}

export class Right<L, A> {
  readonly value: A;

  constructor(value: A) {
    this.value = value;
  }

  isLeft(): this is Left<L, A> {
    return false;
  }

  isRight(): this is Right<L, A> {
    return true;
  }
}

export const left = <L, A>(l: L): Either<L, A> => {
  return new Left(l);
};

export const right = <L, A>(a: A): Either<L, A> => {
  return new Right<L, A>(a);
};
