import { UniqueDomainEntityID } from "./UniqueDomainEntityID";

const isDomainEntity = (v: any): v is DomainEntity<any> => {
  return v instanceof DomainEntity;
};

export abstract class DomainEntity<T> {
  protected readonly _id: UniqueDomainEntityID;
  public readonly props: T;

  constructor(props: T, id?: UniqueDomainEntityID) {
    this._id = id ? id : new UniqueDomainEntityID();
    this.props = props;
  }

  public equals(object?: DomainEntity<T>): boolean {
    if (object == null || object == undefined) {
      return false;
    }

    if (this === object) {
      return true;
    }

    if (!isDomainEntity(object)) {
      return false;
    }

    return this._id.equals(object._id);
  }
}
