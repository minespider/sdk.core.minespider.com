import * as joi from "joi";
import { Result } from "./Result";

export interface GuardArgumentInterface {
  argument: any;
  argumentName: string;
}

export type GuardArgumentCollection = GuardArgumentInterface[];

export class Guard {
  public static greaterThan(
    minValue: number,
    actualValue: number
  ): Result<void> {
    if (actualValue > minValue) {
      return Result.ok();
    }

    return Result.fail(
      new Error(
        `Number given {${actualValue}} is not greater than {${minValue}}`
      )
    );
  }

  public static againstAtLeast(numChars: number, text: string): Result<void> {
    if (text.length >= numChars) {
      return Result.ok();
    }

    return Result.fail(new Error(`Text is not at least ${numChars} chars.`));
  }

  public static againstAtMost(numChars: number, text: string): Result<void> {
    if (text.length <= numChars) {
      return Result.ok();
    }

    return Result.fail(new Error(`Text is greater than ${numChars} chars.`));
  }

  public static againstNullOrUndefined(
    argument: any,
    argumentName: string
  ): Result<void> {
    if (argument === null || argument === undefined) {
      return Result.fail(new Error(`${argumentName} is null or undefined`));
    }

    return Result.ok();
  }

  public static againstNullOrUndefinedBulk(
    args: GuardArgumentCollection
  ): Result<void> {
    for (let arg of args) {
      const result = this.againstNullOrUndefined(
        arg.argument,
        arg.argumentName
      );
      if (result.isFailure) return result;
    }

    return Result.ok();
  }

  public static isOneOf(
    value: any,
    validValues: any[],
    argumentName: string
  ): Result<void> {
    let isValid = false;
    for (let validValue of validValues) {
      if (value === validValue) {
        isValid = true;
      }
    }

    if (isValid) {
      return Result.ok();
    }

    return Result.fail(
      new Error(
        `${argumentName} isn't oneOf the correct types in ${JSON.stringify(
          validValues
        )}. Got "${value}".`
      )
    );
  }

  public static inRange(
    num: number,
    min: number,
    max: number,
    argumentName: string
  ): Result<void> {
    const isInRange = num >= min && num <= max;
    if (!isInRange) {
      return Result.fail(
        new Error(`${argumentName} is not within range ${min} to ${max}.`)
      );
    }

    return Result.ok();
  }

  public static allInRange(
    numbers: number[],
    min: number,
    max: number,
    argumentName: string
  ): Result<void> {
    let failingResult: Result<void> | undefined;

    for (let num of numbers) {
      const numIsInRangeResult = this.inRange(num, min, max, argumentName);

      if (numIsInRangeResult.isFailure) {
        failingResult = numIsInRangeResult;
      }
    }

    if (failingResult) {
      return Result.fail(
        new Error(`${argumentName} is not within the range.`)
      );
    }

    return Result.ok();
  }

  public static validate(value: any, schema: joi.Schema): Result<void> {
    const validationResult = schema.validate(value);

    if (validationResult.error) {
      return Result.fail(new Error(validationResult.error.message));
    }

    return Result.ok();
  }
}
