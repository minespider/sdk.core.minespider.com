import { Result } from "../core/Result";
import { InMemoryFile } from "../domain/File";

export interface FileStorageServiceInterface {
  upload(file: InMemoryFile): Promise<Result<string>>;
  download(fileAddress: string): Promise<Result<Buffer>>;
}
