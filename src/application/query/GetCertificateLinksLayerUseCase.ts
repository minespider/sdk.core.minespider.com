import { Result } from "../../core/Result";
import { GetCertificateLinksLayerQuery } from "./GetCertificateLinksLayerQuery";
import { LinksLayer, LinksLayerDTO } from "../../domain/LinksLayer";
import { FileStorageServiceInterface } from "../FileStorageService";
import { EncryptionServiceInterface } from "../../domain/EncryptionService";

export class GetCertificateLinksLayerUseCase {
  constructor(
    private fileStorageService: FileStorageServiceInterface,
    private encryptionService: EncryptionServiceInterface
  ) {}

  async execute(
    query: GetCertificateLinksLayerQuery
  ): Promise<Result<LinksLayer>> {
    try {
      const linksLayerFile = await this.fileStorageService.download(
        query.linksLayerHash
      );
      const linksLayerEncrypted = linksLayerFile.unwrap();

      const linksLayerDecrypted = await this.encryptionService.decryptWithPassword(
        linksLayerEncrypted,
        query.linksLayerEncryptionKey
      );

      const linksLayerData: LinksLayerDTO = JSON.parse(
        linksLayerDecrypted.toString()
      );

      return Result.ok(
        new LinksLayer({
          links: linksLayerData.links,
          hash: linksLayerData.hash,
        })
      );
    } catch (error: any) {
      return Result.fail(error.message);
    }
  }
}
