import { CertificateId } from "../../domain/CertificateId";
import { Query } from "../Bus";

export class GetCertificatePublicLayerByCertificateIdQuery implements Query {
  static NAME = "GetCertificatePublicLayerByCertificateIdQuery";

  constructor(readonly certificateId: CertificateId) {}

  getName(): string {
    return GetCertificatePublicLayerByCertificateIdQuery.NAME;
  }
}
