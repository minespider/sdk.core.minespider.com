import { Result } from "../../core/Result";
import { GetCertificateTransparencyLayerQuery } from "./GetCertificateTransparencyLayerQuery";
import {
  TransparencyLayer,
  TransparencyLayerDTO,
} from "../../domain/TransparencyLayer";
import { FileStorageServiceInterface } from "../FileStorageService";
import { EncryptionServiceInterface } from "../../domain/EncryptionService";

export class GetCertificateTransparencyLayerUseCase {
  constructor(
    private fileStorageService: FileStorageServiceInterface,
    private encryptionService: EncryptionServiceInterface
  ) {}

  async execute(
    query: GetCertificateTransparencyLayerQuery
  ): Promise<Result<TransparencyLayer>> {
    try {
      const transparencyLayerFile = await this.fileStorageService.download(
        query.transparencyLayerHash
      );
      const transparencyLayerEncrypted = transparencyLayerFile.unwrap();

      const transparencyLayerDecrypted = await this.encryptionService.decryptWithPassword(
        transparencyLayerEncrypted,
        query.transparencyLayerEncryptionKey
      );

      const transparencyLayerData: TransparencyLayerDTO = JSON.parse(
        transparencyLayerDecrypted.toString()
      );

      return Result.ok(
        new TransparencyLayer({
          id: transparencyLayerData.id,
          metadata: transparencyLayerData.metadata,
          files: transparencyLayerData.files,
          upstream: transparencyLayerData.upstream,
        })
      );
    } catch (error: any) {
      return Result.fail(error.message);
    }
  }
}
