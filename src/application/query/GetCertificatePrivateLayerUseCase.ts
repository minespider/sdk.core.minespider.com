import { Result } from "../../core/Result";
import { GetCertificatePrivateLayerQuery } from "./GetCertificatePrivateLayerQuery";
import { PrivateLayer, PrivateLayerDTO } from "../../domain/PrivateLayer";
import { FileStorageServiceInterface } from "../FileStorageService";
import { EncryptionServiceInterface } from "../../domain/EncryptionService";

export class GetCertificatePrivateLayerUseCase {
  constructor(
    private fileStorageService: FileStorageServiceInterface,
    private encryptionService: EncryptionServiceInterface
  ) {}

  async execute(
    query: GetCertificatePrivateLayerQuery
  ): Promise<Result<PrivateLayer>> {
    try {
      const privateLayerFile = await this.fileStorageService.download(
        query.privateLayerHash
      );
      const privateLayerEncrypted = privateLayerFile.unwrap();

      const privateLayerDecrypted = await this.encryptionService.decryptWithPassword(
        privateLayerEncrypted,
        query.privateLayerEncryptionKey
      );

      const privateLayerData: PrivateLayerDTO = JSON.parse(
        privateLayerDecrypted.toString()
      );

      return Result.ok(
        new PrivateLayer({
          metadata: privateLayerData.metadata,
          files: privateLayerData.files,
        })
      );
    } catch (error: any) {
      return Result.fail(error.message);
    }
  }
}
