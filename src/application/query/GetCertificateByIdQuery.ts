import { CertificateId } from "../../domain/CertificateId";
import { Query } from "../Bus";

export class GetCertificateByIdQuery implements Query {
  static NAME = "GetCertificateByIdQuery";

  constructor(readonly certificateId: CertificateId) {}

  getName(): string {
    return GetCertificateByIdQuery.NAME;
  }
}
