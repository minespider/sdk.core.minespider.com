import { Result } from "../../core/Result";
import { GetFileQuery } from "./GetFileQuery";
import { FileStorageServiceInterface } from "../FileStorageService";

export class GetFileUseCase {
  constructor(private fileStorageService: FileStorageServiceInterface) {}

  async execute(query: GetFileQuery): Promise<Result<Buffer>> {
    try {
      return this.fileStorageService.download(query.address);
    } catch (error: any) {
      return Result.fail(error.message);
    }
  }
}
