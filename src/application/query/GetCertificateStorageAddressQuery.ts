import { Query } from "../Bus";

export class GetCertificateStorageAddressQuery implements Query {
  static NAME = "GetCertificateStorageAddressQuery";

  getName(): string {
    return GetCertificateStorageAddressQuery.NAME;
  }
}
