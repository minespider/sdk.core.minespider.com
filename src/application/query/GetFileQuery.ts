export class GetFileQuery {
  static NAME = "GetFileQuery";

  constructor(readonly address: string) {}

  getName(): string {
    return GetFileQuery.NAME;
  }
}
