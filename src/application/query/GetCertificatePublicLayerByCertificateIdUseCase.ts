import { Result } from "../../core/Result";
import { GetCertificatePublicLayerByCertificateIdQuery } from "./GetCertificatePublicLayerByCertificateIdQuery";
import { PublicLayer } from "../../domain/PublicLayer";
import { CertificateServiceInterface } from "../../domain/CertificateService";

export class GetCertificatePublicLayerByCertificateIdUseCase {
  constructor(private certificateService: CertificateServiceInterface) {}

  async execute(
    query: GetCertificatePublicLayerByCertificateIdQuery
  ): Promise<Result<PublicLayer>> {
    try {
      const certificateResult = await this.certificateService.getCertificate(
        query.certificateId
      );
      const certificate = certificateResult.unwrap();

      return Result.ok(certificate.props.publicLayer);
    } catch (error: any) {
      return Result.fail(error.message);
    }
  }
}
