import { Result } from "../../core/Result";
import { GetCertificateStorageAddressQuery } from "./GetCertificateStorageAddressQuery";
import { CertificateServiceInterface } from "../../domain/CertificateService";

export class GetCertificateStorageAddressUseCase {
  constructor(private certificateService: CertificateServiceInterface) {}

  async execute(
    query: GetCertificateStorageAddressQuery
  ): Promise<Result<string>> {
    try {
      return this.certificateService.getCertificatesStorageAddress();
    } catch (error: any) {
      return Result.fail(error.message);
    }
  }
}
