import { Query } from "../Bus";

export class GetCertificatePublicLayerQuery implements Query {
  static NAME = "GetCertificatePublicLayerQuery";

  constructor(readonly publicLayerHash: string) {}

  getName(): string {
    return GetCertificatePublicLayerQuery.NAME;
  }
}
