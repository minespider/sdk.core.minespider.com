import { Result } from "../../core/Result";
import { GetCertificateKeysLayerQuery } from "./GetCertificateKeysLayerQuery";
import { KeysLayer, KeysLayerDTO } from "../../domain/KeysLayer";
import { FileStorageServiceInterface } from "../FileStorageService";
import { EncryptionServiceInterface } from "../../domain/EncryptionService";

export class GetCertificateKeysLayerUseCase {
  constructor(
    private fileStorageService: FileStorageServiceInterface,
    private encryptionService: EncryptionServiceInterface
  ) {}

  async execute(
    query: GetCertificateKeysLayerQuery
  ): Promise<Result<KeysLayer>> {
    try {
      const keysLayerFile = await this.fileStorageService.download(
        query.keysLayerAddress
      );
      const keysLayerEncrypted = keysLayerFile.unwrap();

      const keysLayerDecrypted = await this.encryptionService.decryptWithPrivateKey(
        keysLayerEncrypted,
        query.ownerPrivateKey
      );

      const keysLayerData: KeysLayerDTO = JSON.parse(
        keysLayerDecrypted.toString()
      );

      return Result.ok(
        new KeysLayer(
          keysLayerData.recipient.encryption_key,
          keysLayerData.private.encryption_key,
          keysLayerData.transparency.encryption_key,
          keysLayerData.links.encryption_key
        )
      );
    } catch (error: any) {
      return Result.fail(error.message);
    }
  }
}
