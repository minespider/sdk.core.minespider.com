export class GetCertificateTransparencyLayerQuery {
  static NAME = "GetCertificateTransparencyLayerQuery";

  constructor(
    readonly transparencyLayerHash: string,
    readonly transparencyLayerEncryptionKey: string
  ) {}

  getName(): string {
    return GetCertificateTransparencyLayerQuery.NAME;
  }
}
