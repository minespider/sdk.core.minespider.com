import { Result } from "../../core/Result";
import { GetCertificateRecipientLayerQuery } from "./GetCertificateRecipientLayerQuery";
import {
  RecipientLayer,
  RecipientLayerDTO,
} from "../../domain/RecipientLayer";
import { FileStorageServiceInterface } from "../FileStorageService";
import { EncryptionServiceInterface } from "../../domain/EncryptionService";

export class GetCertificateRecipientLayerUseCase {
  constructor(
    private fileStorageService: FileStorageServiceInterface,
    private encryptionService: EncryptionServiceInterface
  ) {}

  async execute(
    query: GetCertificateRecipientLayerQuery
  ): Promise<Result<RecipientLayer>> {
    try {
      const recipientLayerFile = await this.fileStorageService.download(
        query.recipientLayerHash
      );
      const recipientLayerEncrypted = recipientLayerFile.unwrap();

      const recipientLayerDecrypted = await this.encryptionService.decryptWithPassword(
        recipientLayerEncrypted,
        query.recipientLayerEncryptionKey
      );

      const recipientLayerData: RecipientLayerDTO = JSON.parse(
        recipientLayerDecrypted.toString()
      );

      return Result.ok(
        new RecipientLayer(
          recipientLayerData.id,
          Buffer.from(recipientLayerData.publicKey)
        )
      );
    } catch (error: any) {
      return Result.fail(error.message);
    }
  }
}
