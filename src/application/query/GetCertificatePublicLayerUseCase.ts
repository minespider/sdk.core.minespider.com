import { Result } from "../../core/Result";
import { GetCertificatePublicLayerQuery } from "./GetCertificatePublicLayerQuery";
import { CERTIFICATE_ACTIONS, CERTIFICATE_TYPES, PublicLayer } from "../../domain/PublicLayer";
import { FileStorageServiceInterface } from "../FileStorageService";
import { CertificateId } from "../../domain/CertificateId";
import { UniqueDomainEntityID } from "../../core/UniqueDomainEntityID";

export class GetCertificatePublicLayerUseCase {
  constructor(private fileStorageService: FileStorageServiceInterface) {}

  async execute(
    query: GetCertificatePublicLayerQuery
  ): Promise<Result<PublicLayer>> {
    try {
      const publicLayerFile = await this.fileStorageService.download(
        query.publicLayerHash
      );
      const publicLayerData = JSON.parse(publicLayerFile.unwrap().toString());

      return Result.ok(
        new PublicLayer({
          id: CertificateId.create(
            new UniqueDomainEntityID(publicLayerData.id)
          ).unwrap(),
          code: publicLayerData.code,
          version: publicLayerData.version,
          type: (publicLayerData.type === CERTIFICATE_TYPES.SHIPMENT ? publicLayerData.type === CERTIFICATE_TYPES.PRODUCT_PASSPORT : publicLayerData.type) as CERTIFICATE_TYPES,
          action: publicLayerData.action as CERTIFICATE_ACTIONS,
          owner: publicLayerData.owner,
          recipient: publicLayerData.recipient,
          parent: publicLayerData.parent
            ? CertificateId.create(
                new UniqueDomainEntityID(publicLayerData.parent)
              ).unwrap()
            : null,
          metadata: publicLayerData.metadata,
          materials: publicLayerData.materials,
          privateLayerAddress: publicLayerData.layers.private,
          transparencyLayerAddress: publicLayerData.layers.transparency,
          recipientLayerAddress: publicLayerData.layers.recipient,
          linksLayerAddress: publicLayerData.layers.links,
          creationTimestamp: publicLayerData.creation_timestamp,
          files: publicLayerData.files,
        })
      );
    } catch (error: any) {
      return Result.fail(error.message);
    }
  }
}
