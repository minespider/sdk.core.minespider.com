export class GetCertificateRecipientLayerQuery {
  static NAME = "GetCertificateRecipientLayerQuery";

  constructor(
    readonly recipientLayerHash: string,
    readonly recipientLayerEncryptionKey: string
  ) {}

  getName(): string {
    return GetCertificateRecipientLayerQuery.NAME;
  }
}
