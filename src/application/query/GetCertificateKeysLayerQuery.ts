import { CertificateOwnerPrivateKey } from "../../domain/CertificateOwner";

export class GetCertificateKeysLayerQuery {
  static NAME = "GetCertificateKeysLayerQuery";

  constructor(
    readonly keysLayerAddress: string,
    readonly ownerPrivateKey: CertificateOwnerPrivateKey
  ) {}

  getName(): string {
    return GetCertificateKeysLayerQuery.NAME;
  }
}
