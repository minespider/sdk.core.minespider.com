export class GetCertificatePrivateLayerQuery {
  static NAME = "GetCertificatePrivateLayerQuery";

  constructor(
    readonly privateLayerHash: string,
    readonly privateLayerEncryptionKey: string
  ) {}

  getName(): string {
    return GetCertificatePrivateLayerQuery.NAME;
  }
}
