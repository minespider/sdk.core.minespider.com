export class GetCertificateLinksLayerQuery {
  static NAME = "GetCertificateLinksLayerQuery";

  constructor(
    readonly linksLayerHash: string,
    readonly linksLayerEncryptionKey: string
  ) {}

  getName(): string {
    return GetCertificateLinksLayerQuery.NAME;
  }
}
