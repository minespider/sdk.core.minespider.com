import { Result } from "../../core/Result";
import { GetCertificateByIdQuery } from "./GetCertificateByIdQuery";
import { CertificateServiceInterface } from "../../domain/CertificateService";
import { Certificate } from "../../domain/Certificate";

export class GetCertificateByIdUseCase {
  constructor(private certificateService: CertificateServiceInterface) {}

  async execute(query: GetCertificateByIdQuery): Promise<Result<Certificate>> {
    try {
      return this.certificateService.getCertificate(query.certificateId);
    } catch (error: any) {
      return Result.fail(error.message);
    }
  }
}
