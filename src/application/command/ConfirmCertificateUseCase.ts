import { ConfirmCertificateCommand } from "./ConfirmCertificateCommand";
import { Certificate } from "../../domain/Certificate";
import { Result } from "../../core/Result";
import { CertificateServiceInterface } from "../../domain/CertificateService";

export class ConfirmCertificateUseCase {
  constructor(private certificateService: CertificateServiceInterface) {}

  async execute(
    command: ConfirmCertificateCommand
  ): Promise<Result<[string, string, Certificate]>> {
    try {
      return await this.certificateService.confirmCertificate(command.props);
    } catch (error: any) {
      return Result.fail(error.message);
    }
  }
}
