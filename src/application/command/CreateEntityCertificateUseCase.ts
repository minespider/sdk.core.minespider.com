import { CreateEntityCertificateCommand } from "./CreateEntityCertificateCommand";
import { Result } from "../../core/Result";
import {
  CertificateServiceInterface,
  CreateCertificateResponse
} from "../../domain/CertificateService";
import { CERTIFICATE_TYPES } from "../../domain/PublicLayer";

export class CreateEntityCertificateUseCase {
  constructor(private certificateService: CertificateServiceInterface) {
  }

  async execute(
    command: CreateEntityCertificateCommand
  ): Promise<Result<CreateCertificateResponse>> {
    try {
      const certificateResult = await this.certificateService.createCertificate(
        command.props,
        CERTIFICATE_TYPES.ENTITY
      );

      return Result.ok(certificateResult.unwrap());
    } catch (error: any) {
      console.error(`Unable to create entity certificate ${command.props.public.id} because ${error.message}`);
      return Result.fail(error.message);
    }
  }
}
