import { EditCertificateCommand } from "./EditCertificateCommand";
import { Result } from "../../core/Result";
import {
  CertificateServiceInterface,
  EditCertificateResponse,
} from "../../domain/CertificateService";

export class EditCertificateUseCase {
  constructor(private certificateService: CertificateServiceInterface) {}

  async execute(
    command: EditCertificateCommand
  ): Promise<Result<EditCertificateResponse>> {
    try {
      const certificateResult = await this.certificateService.editCertificate(
        command.props,
        command.account
      );
      const response = certificateResult.unwrap();

      return Result.ok(response);
    } catch (error: any) {
      return Result.fail(error);
    }
  }
}
