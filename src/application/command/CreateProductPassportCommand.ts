import { RecipientId, RecipientPublicKey } from "../../domain/Recipient";
import { Command } from "../Bus";
import { CreateCertificateProps } from "../../domain/Certificate";

export interface CreateProductPassportProps
  extends CreateCertificateProps {
  recipient: {
    id: RecipientId;
    publicKey: RecipientPublicKey;
  };
}

export class CreateProductPassportCommand implements Command {
  static NAME = "CreateProductPassportCommand";

  readonly props: CreateProductPassportProps;

  constructor(props: CreateProductPassportProps) {
    this.props = props;
  }

  getName(): string {
    return CreateProductPassportCommand.NAME;
  }
}
