import { Command } from "../Bus";
import { ConfirmCertificateProps } from "../../domain/Certificate";

export class ConfirmCertificateCommand implements Command {
  static NAME = "ConfirmCertificateCommand";

  constructor(readonly props: ConfirmCertificateProps) {}

  getName(): string {
    return ConfirmCertificateCommand.NAME;
  }
}
