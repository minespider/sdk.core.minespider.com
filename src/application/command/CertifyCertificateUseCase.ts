import { CertifyCertificateCommand } from "./CertifyCertificateCommand";
import { Certificate } from "../../domain/Certificate";
import { Result } from "../../core/Result";
import { CertificateServiceInterface } from "../../domain/CertificateService";

export class CertifyCertificateUseCase {
  constructor(private certificateService: CertificateServiceInterface) {}

  async execute(
    command: CertifyCertificateCommand
  ): Promise<Result<Certificate>> {
    try {
      const certificateResult = await this.certificateService.certifyCertificate(
        command.props
      );
      const certificate = certificateResult.unwrap();

      return Result.ok(certificate);
    } catch (error: any) {
      return Result.fail(error.message);
    }
  }
}
