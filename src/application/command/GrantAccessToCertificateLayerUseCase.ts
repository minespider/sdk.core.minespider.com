import { GrantAccessToCertificateLayerCommand } from "./GrantAccessToCertificateLayerCommand";
import { Result } from "../../core/Result";
import { CertificateServiceInterface } from "../../domain/CertificateService";
import { Bus } from "../Bus";
import { GetCertificatePublicLayerQuery } from "../query/GetCertificatePublicLayerQuery";
import { PublicLayer } from "../../domain/PublicLayer";

export class GrantAccessToCertificateLayerUseCase {
  constructor(
    private certificateService: CertificateServiceInterface,
    private bus: Bus
  ) {}

  async execute(
    command: GrantAccessToCertificateLayerCommand
  ): Promise<Result<string>> {
    try {
      const publicLayerResult = await this.bus.execute<PublicLayer>(
        new GetCertificatePublicLayerQuery(command.publicLayerAddress)
      );
      const publicLayer = publicLayerResult.unwrap();

      const keysLayerHashResult = await this.certificateService.grantAccessToKeysLayer(
        publicLayer.owner.keys,
        command.ownerAccount,
        command.grantAccessTo.publicKey
      );
      const keysLayerHash = keysLayerHashResult.unwrap();

      return Result.ok(keysLayerHash);
    } catch (error: any) {
      return Result.fail(error.message);
    }
  }
}
