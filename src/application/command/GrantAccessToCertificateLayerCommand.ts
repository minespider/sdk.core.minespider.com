import { Command } from "../Bus";
import { EthereumAccount } from "../../core/EthereumAccount";

export class GrantAccessToCertificateLayerCommand implements Command {
  static NAME = "GrantAccessToCertificateLayerCommand";

  constructor(
    readonly publicLayerAddress: string,
    readonly ownerAccount: EthereumAccount,
    readonly grantAccessTo: {
      id: string;
      publicKey: string;
    }
  ) {}

  getName(): string {
    return GrantAccessToCertificateLayerCommand.NAME;
  }
}
