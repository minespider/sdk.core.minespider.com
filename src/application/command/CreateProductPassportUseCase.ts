import { CreateProductPassportCommand } from "./CreateProductPassportCommand";
import { Result } from "../../core/Result";
import { CertificateServiceInterface, CreateCertificateResponse } from "../../domain/CertificateService";
import { CERTIFICATE_TYPES } from "../../domain/PublicLayer";

export class CreateProductPassportUseCase {
  constructor(private certificateService: CertificateServiceInterface) {}

  async execute(
    command: CreateProductPassportCommand
  ): Promise<Result<CreateCertificateResponse>> {
    try {
      const certificateResult = await this.certificateService.createCertificate(
        command.props,
        CERTIFICATE_TYPES.PRODUCT_PASSPORT
      );
      const response = certificateResult.unwrap();

      return Result.ok(response);
    } catch (error: any) {
      console.error(`Unable to create product passport ${command.props.public.id} because ${error.message}`);
      return Result.fail(error.message);
    }
  }
}
