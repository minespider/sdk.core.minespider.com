import { Command } from "../Bus";
import { CertifyCertificateProps } from "../../domain/Certificate";

export class CertifyCertificateCommand implements Command {
  static NAME = "CertifyCertificateCommand";

  constructor(readonly props: CertifyCertificateProps) {}

  getName(): string {
    return CertifyCertificateCommand.NAME;
  }
}
