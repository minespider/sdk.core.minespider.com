import { Command } from "../Bus";
import { EditCertificateProps } from "../../domain/Certificate";
import { EthereumAccount } from "../../core/EthereumAccount";

export class EditCertificateCommand implements Command {
  static NAME = "EditCertificateCommand";

  constructor(
    readonly props: EditCertificateProps,
    readonly account: EthereumAccount
  ) {}

  getName(): string {
    return EditCertificateCommand.NAME;
  }
}
