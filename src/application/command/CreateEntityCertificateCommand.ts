import { Command } from "../Bus";
import { CreateCertificateProps } from "../../domain/Certificate";

export interface CreateEntityCertificateProps extends CreateCertificateProps {}

export class CreateEntityCertificateCommand implements Command {
  static NAME = "CreateEntityCertificateCommand";

  readonly props: CreateEntityCertificateProps;

  constructor(props: CreateEntityCertificateProps) {
    this.props = props;
  }

  getName(): string {
    return CreateEntityCertificateCommand.NAME;
  }
}
