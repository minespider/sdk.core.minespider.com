import { RecipientLayer } from "../domain/RecipientLayer";
import {
  Certificate,
  CertifyCertificateProps,
  ConfirmCertificateProps,
  CreateCertificateProps,
  EditCertificateProps,
  GrantAccessToCertificateProps
} from "../domain/Certificate";
import {
  CERTIFICATE_ACTIONS,
  CERTIFICATE_TYPES,
  PublicLayer,
  PublicLayerDTO,
  PublicLayerFiles
} from "../domain/PublicLayer";
import { PrivateLayer, PrivateLayerFiles } from "../domain/PrivateLayer";
import { Result } from "../core/Result";
import { CertificateId } from "../domain/CertificateId";
import { KeysLayer, SymetricEncryptionKey } from "../domain/KeysLayer";
import { TransparencyLayer, TransparencyLayerFiles } from "../domain/TransparencyLayer";
import { FileReference, InMemoryFile } from "../domain/File";
import { GetCertificatePublicLayerQuery } from "./query/GetCertificatePublicLayerQuery";
import { EncryptionServiceInterface } from "../domain/EncryptionService";
import { KeysLayerFactory } from "../domain/KeysLayerFactory";
import { TransparencyLayerFactory } from "../domain/TransparencyLayerFactory";
import { LinksLayerFactory } from "../domain/LinksLayerFactory";
import { FileStorageServiceInterface } from "./FileStorageService";
import {
  CertificateServiceInterface,
  CreateCertificateResponse,
  EditCertificateResponse
} from "../domain/CertificateService";
import { BlockchainServiceInterface } from "./BlockchainService";
import { Bus } from "./Bus";
import { EthereumAccount } from "../core/EthereumAccount";
import { GetCertificateKeysLayerQuery } from "./query/GetCertificateKeysLayerQuery";
import { GetCertificateRecipientLayerQuery } from "./query/GetCertificateRecipientLayerQuery";
import { GetCertificateTransparencyLayerQuery } from "./query/GetCertificateTransparencyLayerQuery";
import { UniqueDomainEntityID } from "../core/UniqueDomainEntityID";
import { LinksLayer } from "../domain/LinksLayer";

export class CertificateService implements CertificateServiceInterface {
  constructor(
    private encryptionService: EncryptionServiceInterface,
    private keysLayerFactory: KeysLayerFactory,
    private transparencyLayerFactory: TransparencyLayerFactory,
    private linksLayerFactory: LinksLayerFactory,
    private fileStorageService: FileStorageServiceInterface,
    private blockchainService: BlockchainServiceInterface,
    private bus: Bus
  ) {
  }

  async createCertificate(
    props: CreateCertificateProps,
    type: CERTIFICATE_TYPES
  ): Promise<Result<CreateCertificateResponse>> {
    console.log(`Getting Ethereum account for ${props.owner.id} who is owner of certificate ${props.public.id}`);
    const ownerAccount = EthereumAccount.create(
      props.owner.privateKey.toString()
    ).unwrap();

    if (props.owner.id !== ownerAccount.props.address.toString()) {
      return Result.fail(
        new Error("Owner id invalid for does not match the given private key")
      );
    }

    const keysLayer = await this.keysLayerFactory.create();
    console.log(`Creating keys layer for ${props.owner.id} who is owner of certificate ${props.public.id}`);
    const ownerKeysLayerHash = await this.createKeysLayer(
      keysLayer,
      ownerAccount.publicKey
    );
    console.log(`Creating keys layer for ${props.recipient ? props.recipient.id : props.owner.id} who is recipient of certificate ${props.public.id}`);
    const recipientKeysLayerHash = await this.createKeysLayer(
      keysLayer,
      props.recipient ? props.recipient.publicKey : ownerAccount.publicKey
    );

    let parentKeysLayerHash: string | null = null;

    if (props.public.parent) {
      console.log(`Getting public layer for ${props.public.parent} which is parent certificate of ${props.public.id} certificate`);
      const parentPublicLayerResult = await this.bus.execute<PublicLayer>(
        new GetCertificatePublicLayerQuery(props.public.parent)
      );
      const parentPublicLayer = parentPublicLayerResult.unwrap();
      parentKeysLayerHash = parentPublicLayer.owner.keys;
    }

    console.log(`Getting recipient layer for ${props.recipient
      ? props.recipient.id
      : ownerAccount.props.address.toString()} who is recipient of ${props.public.id} certificate`);
    const recipientLayer = new RecipientLayer(
      props.recipient
        ? props.recipient.id
        : ownerAccount.props.address.toString(),
      props.recipient ? props.recipient.publicKey : ownerAccount.publicKey
    );

    console.log(`Encrypting recipient layer for ${props.recipient ? props.recipient.id : props.owner.id} who is recipient of ${props.public.id} certificate`);
    const recipientLayerHash = await this.encryptLayer(
      recipientLayer,
      keysLayer.recipientEncryptionKey
    );

    console.log(`Getting transparency layers from links for certificate ${props.public.id}`);
    const upstream =
      await this.transparencyLayerFactory.getTransparencyLayersFromLinks(
        props.links,
        ownerAccount
      );
    console.log(`Getting transparency file references for certificate ${props.public.id}`);
    const transparencyFileReferences: TransparencyLayerFiles =
      await this.prepareFiles(
        props.transparency.files,
        keysLayer.transparencyEncryptionKey
      );
    console.log(`Getting transparency layer metadata for certificate ${props.public.id}`);
    const transparencyLayer = new TransparencyLayer({
      id: props.public.id.toString(),
      metadata: props.transparency.metadata,
      files: transparencyFileReferences,
      upstream
    });
    console.log(`Getting transparency layer hash for certificate ${props.public.id}`);
    const transparencyLayerHash = await this.encryptLayer(
      transparencyLayer,
      keysLayer.transparencyEncryptionKey
    );

    console.log(`Getting links layer for certificate ${props.public.id}`);
    const linksLayerResult = await this.linksLayerFactory.create(props.links);
    console.log(`Encrypting links layer for certificate ${props.public.id}`);
    const linksLayerHash = await this.encryptLayer(
      linksLayerResult.unwrap(),
      keysLayer.linksKey
    );

    console.log(`Preparing public layer file references for certificate ${props.public.id}`);
    const publicFileReferences: PublicLayerFiles = await this.prepareFiles(
      props.public.files
    );

    console.log(`Preparing private layer file references for certificate ${props.public.id}`);
    const privateFileReferences: PrivateLayerFiles = await this.prepareFiles(
      props.private.files,
      keysLayer.privateEncryptionKey
    );

    console.log(`Preparing private file references in private metadata layer for certificate ${props.public.id}`);
    const privateLayer = new PrivateLayer({
      metadata: props.private.metadata,
      files: privateFileReferences
    });
    console.log(`Encrypting private file references in private metadata layer for certificate ${props.public.id}`);
    const privateLayerHash = await this.encryptLayer(
      privateLayer,
      keysLayer.privateEncryptionKey
    );

    let parentCertificateId: CertificateId | null = null;

    if (props.public.parent) {
      console.log(`Encrypting private file references in private metadata layer for certificate ${props.public.id}`);
      const parentPublicLayerResult = await this.bus.execute<PublicLayer>(
        new GetCertificatePublicLayerQuery(props.public.parent)
      );
      const publicLayer = parentPublicLayerResult.unwrap();

      parentCertificateId = publicLayer.id;
    }

    console.log(`Preparing public layer for certificate ${props.public.id}`);
    const publicLayer = new PublicLayer({
      id: props.public.id,
      code: props.public.code,
      version: props.public.version,
      type: type === CERTIFICATE_TYPES.PRODUCT_PASSPORT ? CERTIFICATE_TYPES.PRODUCT_PASSPORT : CERTIFICATE_TYPES.ENTITY,
      action: CERTIFICATE_ACTIONS.CREATED,
      owner: {
        id: ownerAccount.props.address.toString(),
        keys: ownerKeysLayerHash,
        publicKey: ownerAccount.publicKey
      },
      recipient: {
        keys: recipientKeysLayerHash
      },
      parent: parentCertificateId,
      metadata: props.public.metadata,
      materials: [],
      privateLayerAddress: privateLayerHash,
      transparencyLayerAddress: transparencyLayerHash,
      recipientLayerAddress: recipientLayerHash,
      linksLayerAddress: linksLayerHash,
      creationTimestamp: new Date().getTime(),
      files: publicFileReferences
    });

    console.log(`Getting public layer buffer for certificate ${props.public.id}`);
    const publicLayerContent = Buffer.from(
      JSON.stringify(publicLayer.toDTO())
    );
    console.log(`Getting public layer hash for certificate ${props.public.id}`);
    const publicLayerHash = this.encryptionService.hash(publicLayerContent);
    console.log(`Uploading public layer for certificate ${props.public.id}`);
    const publicLayerUploadResult = await this.fileStorageService.upload({
      checksum: publicLayerHash,
      content: publicLayerContent,
      type: "text/plain",
      name: publicLayerHash
    });
    publicLayerUploadResult.unwrap();

    console.log(`Preparing certificate ${props.public.id} for saving blockchain`);
    const certificateResult = Certificate.create({
      id: props.public.id,
      publicLayer,
      publicLayerHash
    });
    const certificate = certificateResult.unwrap();

    console.log(`Creating certificate ${props.public.id} in blockchain`);
    const createCertificateBlockchainResult =
      await this.blockchainService.createCertificate(
        ownerAccount.privateKey.toString(),
        {
          id: certificate.certificateId.id.toString(),
          code: certificate.props.publicLayer.code,
          version: certificate.props.publicLayer.version,
          parent: certificate.props.publicLayer.parent
            ? certificate.props.publicLayer.parent.id.toString()
            : "",
          creationTimestamp: certificate.props.publicLayer.creationTimestamp,
          publicLayerHash: certificate.props.publicLayerHash,
          certificateHash: this.encryptionService.hash(certificate.toDTO())
        }
      );

    if (createCertificateBlockchainResult.isFailure) {
      console.error(`Failed to save certificate ${props.public.id} in blockchain because ${createCertificateBlockchainResult.error.message}`);
      createCertificateBlockchainResult.throw();
    }

    return Result.ok({
      certificate: certificate,
      transaction: createCertificateBlockchainResult.unwrap()
    });
  }

  async editCertificate(
    props: EditCertificateProps,
    ownerAccount: EthereumAccount
  ): Promise<Result<EditCertificateResponse>> {
    const parentPublicLayerResult = await this.bus.execute<PublicLayer>(
      new GetCertificatePublicLayerQuery(props.parentPublicLayer)
    );
    const parentPublicLayer = parentPublicLayerResult.unwrap();

    if (parentPublicLayer.owner.id !== ownerAccount.props.address.toString()) {
      return Result.fail(
        new Error("Only the owner can edit this certificate")
      );
    }

    const parentKeysLayerResult = await this.bus.execute<KeysLayer>(
      new GetCertificateKeysLayerQuery(
        parentPublicLayer.owner.keys,
        ownerAccount.privateKey
      )
    );
    const parentKeysLayer = parentKeysLayerResult.unwrap();

    const parentRecipientLayerResult = await this.bus.execute<RecipientLayer>(
      new GetCertificateRecipientLayerQuery(
        parentPublicLayer.recipientLayerAddress,
        parentKeysLayer.recipientEncryptionKey
      )
    );
    const parentRecipientLayer = parentRecipientLayerResult.unwrap();

    const keysLayer = await this.keysLayerFactory.create();
    const ownerKeysLayerHash = await this.createKeysLayer(
      keysLayer,
      ownerAccount.publicKey
    );
    const recipientKeysLayerHash = await this.createKeysLayer(
      keysLayer,
      parentRecipientLayer.publicKey
    );

    const recipientLayer = new RecipientLayer(
      parentRecipientLayer.id,
      parentRecipientLayer.publicKey
    );
    const recipientLayerHash = await this.encryptLayer(
      recipientLayer,
      keysLayer.recipientEncryptionKey
    );

    const transparencyFileReferences: PrivateLayerFiles =
      await this.prepareFiles(
        props.transparency.files,
        keysLayer.transparencyEncryptionKey
      );

    const links = props.links;
    links.push({
      id: parentPublicLayer.id.toString(),
      undisclosed: false
    });

    const upstream =
      await this.transparencyLayerFactory.getTransparencyLayersFromLinks(
        links,
        ownerAccount
      );

    const transparencyLayer = new TransparencyLayer({
      id: props.public.id.toString(),
      metadata: props.transparency.metadata,
      files: transparencyFileReferences,
      upstream: upstream
    });
    const transparencyLayerHash = await this.encryptLayer(
      transparencyLayer,
      keysLayer.transparencyEncryptionKey
    );

    const linksLayerResult = await this.linksLayerFactory.create(props.links);
    const linksLayerHash = await this.encryptLayer(
      linksLayerResult.unwrap(),
      keysLayer.linksKey
    );

    const publicFileReferences: PublicLayerFiles = await this.prepareFiles(
      props.public.files
    );

    const privateFileReferences: PrivateLayerFiles = await this.prepareFiles(
      props.private.files,
      keysLayer.privateEncryptionKey
    );

    const privateLayer = new PrivateLayer({
      metadata: props.private.metadata,
      files: privateFileReferences
    });
    const privateLayerHash = await this.encryptLayer(
      privateLayer,
      keysLayer.privateEncryptionKey
    );

    const publicLayer = new PublicLayer({
      id: props.public.id,
      code: props.public.code,
      version: props.public.version,
      type: parentPublicLayer.type,
      action: CERTIFICATE_ACTIONS.EDITED,
      owner: {
        id: parentPublicLayer.owner.id,
        keys: ownerKeysLayerHash,
        publicKey: parentPublicLayer.owner.publicKey
      },
      recipient: {
        keys: recipientKeysLayerHash
      },
      parent: parentPublicLayer.id,
      metadata: props.public.metadata,
      materials: [],
      privateLayerAddress: privateLayerHash,
      transparencyLayerAddress: transparencyLayerHash,
      recipientLayerAddress: recipientLayerHash,
      linksLayerAddress: linksLayerHash,
      creationTimestamp: new Date().getTime(),
      files: publicFileReferences
    });

    const publicLayerContent = Buffer.from(
      JSON.stringify(publicLayer.toDTO())
    );
    const publicLayerHash = this.encryptionService.hash(publicLayerContent);
    const publicLayerUploadResult = await this.fileStorageService.upload({
      checksum: publicLayerHash,
      content: publicLayerContent,
      type: "text/plain",
      name: publicLayerHash
    });
    publicLayerUploadResult.unwrap();

    const certificateResult = Certificate.create({
      id: props.public.id,
      publicLayer,
      publicLayerHash
    });
    const certificate = certificateResult.unwrap();

    const createCertificateBlockchainResult =
      await this.blockchainService.createCertificate(
        ownerAccount.privateKey.toString(),
        {
          id: certificate.id.toString(),
          code: certificate.props.publicLayer.code,
          version: certificate.props.publicLayer.version,
          parent: parentPublicLayer.id.toString(),
          creationTimestamp: certificate.props.publicLayer.creationTimestamp,
          publicLayerHash: certificate.props.publicLayerHash,
          certificateHash: this.encryptionService.hash(certificate.toDTO())
        }
      );

    if (createCertificateBlockchainResult.isFailure) {
      createCertificateBlockchainResult.throw();
    }

    return Result.ok({
      certificate: certificate,
      transaction: createCertificateBlockchainResult.unwrap()
    });
  }

  async confirmCertificate(
    props: ConfirmCertificateProps
  ): Promise<Result<[string, string, Certificate]>> {
    const parentPublicLayerResult = await this.bus.execute<PublicLayer>(
      new GetCertificatePublicLayerQuery(props.parentPublicLayer)
    );
    const parentPublicLayer = parentPublicLayerResult.unwrap();

    const parentKeysLayerResult = await this.bus.execute<KeysLayer>(
      new GetCertificateKeysLayerQuery(
        props.keysLayerAddress,
        props.confirmedByAccount.privateKey
      )
    );
    const parentKeysLayer = parentKeysLayerResult.unwrap();

    const parentRecipientLayerResult = await this.bus.execute<RecipientLayer>(
      new GetCertificateRecipientLayerQuery(
        parentPublicLayer.recipientLayerAddress,
        parentKeysLayer.recipientEncryptionKey
      )
    );
    const parentRecipientLayer = parentRecipientLayerResult.unwrap();
    const keysLayer = await this.keysLayerFactory.create();
    const confirmedByKeysLayerHash = await this.createKeysLayer(
      keysLayer,
      props.confirmedByAccount.publicKey
    );
    const ownerKeysLayerHash = await this.createKeysLayer(
      keysLayer,
      parentPublicLayer.owner.publicKey
    );
    const recipientKeysLayerHash = await this.createKeysLayer(
      keysLayer,
      parentRecipientLayer.publicKey
    );

    const recipientLayer = new RecipientLayer(
      parentRecipientLayer.id,
      parentRecipientLayer.publicKey
    );
    const recipientLayerHash = await this.encryptLayer(
      recipientLayer,
      keysLayer.recipientEncryptionKey
    );

    const transparencyLayer = new TransparencyLayer({
      id: null,
      metadata: {},
      files: {},
      upstream: []
    });
    const transparencyLayerHash = await this.encryptLayer(
      transparencyLayer,
      keysLayer.transparencyEncryptionKey
    );

    const linksLayerResult = await this.linksLayerFactory.create([]);
    const linksLayerHash = await this.encryptLayer(
      linksLayerResult.unwrap(),
      keysLayer.linksKey
    );

    const privateLayer = new PrivateLayer({
      metadata: {},
      files: {}
    });
    const privateLayerHash = await this.encryptLayer(
      privateLayer,
      keysLayer.privateEncryptionKey
    );

    const publicLayer = new PublicLayer({
      id: props.newCertificateId,
      code: props.newCertificateCode,
      version: parentPublicLayer.version + 1,
      type: parentPublicLayer.type,
      action: CERTIFICATE_ACTIONS.CONFIRMED,
      owner: {
        id: props.confirmedByAccount.props.address.toString(),
        publicKey: props.confirmedByAccount.publicKey,
        keys: confirmedByKeysLayerHash
      },
      recipient: recipientKeysLayerHash
        ? {
          keys: recipientKeysLayerHash
        }
        : undefined,
      parent: parentPublicLayer.id,
      metadata: {},
      materials: [],
      privateLayerAddress: privateLayerHash,
      transparencyLayerAddress: transparencyLayerHash,
      recipientLayerAddress: recipientLayerHash,
      linksLayerAddress: linksLayerHash,
      creationTimestamp: new Date().getTime(),
      files: {}
    });

    const publicLayerContent = Buffer.from(
      JSON.stringify(publicLayer.toDTO())
    );
    const publicLayerHash = this.encryptionService.hash(publicLayerContent);
    const publicLayerUploadResult = await this.fileStorageService.upload({
      checksum: publicLayerHash,
      content: publicLayerContent,
      type: "text/plain",
      name: publicLayerHash
    });
    publicLayerUploadResult.unwrap();

    const certificateResult = Certificate.create({
      id: props.newCertificateId,
      publicLayer,
      publicLayerHash
    });
    const certificate = certificateResult.unwrap();

    const createCertificateBlockchainResult =
      await this.blockchainService.createCertificate(
        props.confirmedByAccount.privateKey.toString(),
        {
          id: certificate.id.toString(),
          code: certificate.props.publicLayer.code,
          version: certificate.props.publicLayer.version,
          parent: parentPublicLayer.id.toString(),
          creationTimestamp: certificate.props.publicLayer.creationTimestamp,
          publicLayerHash: certificate.props.publicLayerHash,
          certificateHash: this.encryptionService.hash(certificate.toDTO())
        }
      );

    if (createCertificateBlockchainResult.isFailure) {
      createCertificateBlockchainResult.throw();
    }

    return Result.ok([
      ownerKeysLayerHash,
      confirmedByKeysLayerHash,
      certificate
    ]);
  }

  async grantAccessToCertificate(
    props: GrantAccessToCertificateProps,
    ownerAccount: EthereumAccount
  ): Promise<Result<Certificate>> {
    const keysLayer = props.keysLayer;
    const parentPublicLayerResult = await this.bus.execute<PublicLayer>(
      new GetCertificatePublicLayerQuery(props.parentPublicLayer)
    );
    const parentPublicLayer = parentPublicLayerResult.unwrap();

    const parentKeysLayerResult = await this.bus.execute<KeysLayer>(
      new GetCertificateKeysLayerQuery(
        props.parentKeysLayerHash,
        ownerAccount.privateKey
      )
    );
    const parentKeysLayer = parentKeysLayerResult.unwrap();

    const parentRecipientLayerResult = await this.bus.execute<RecipientLayer>(
      new GetCertificateRecipientLayerQuery(
        parentPublicLayer.recipientLayerAddress,
        parentKeysLayer.recipientEncryptionKey
      )
    );
    const parentRecipientLayer = parentRecipientLayerResult.unwrap();

    const ownerKeysLayerHash = await this.createKeysLayer(
      keysLayer,
      parentPublicLayer.owner.publicKey
    );
    const recipientKeysLayerHash = await this.createKeysLayer(
      keysLayer,
      parentRecipientLayer.publicKey
    );
    const recipientLayer = new RecipientLayer(
      parentRecipientLayer.id,
      parentRecipientLayer.publicKey
    );
    const recipientLayerHash = await this.encryptLayer(
      recipientLayer,
      keysLayer.recipientEncryptionKey
    );

    const linksLayerResult = await this.linksLayerFactory.create([]);
    const linksLayerHash = await this.encryptLayer(
      linksLayerResult.unwrap(),
      keysLayer.linksKey
    );

    const transparencyLayer = new TransparencyLayer({
      id: null,
      metadata: {},
      files: {},
      upstream: []
    });
    const transparencyLayerHash = await this.encryptLayer(
      transparencyLayer,
      keysLayer.transparencyEncryptionKey
    );

    const privateLayer = new PrivateLayer({
      metadata: {},
      files: {}
    });
    const privateLayerHash = await this.encryptLayer(
      privateLayer,
      keysLayer.privateEncryptionKey
    );

    const publicLayer = new PublicLayer({
      id: props.newCertificateId,
      code: props.newCertificateCode,
      version: parentPublicLayer.version + 1,
      type: parentPublicLayer.type,
      action: parentPublicLayer.action,
      owner: {
        id: parentPublicLayer.owner.id,
        keys: ownerKeysLayerHash,
        publicKey: parentPublicLayer.owner.publicKey
      },
      recipient: recipientKeysLayerHash
        ? {
          keys: recipientKeysLayerHash
        }
        : undefined,
      parent: parentPublicLayer.id,
      metadata: {},
      materials: [],
      privateLayerAddress: privateLayerHash,
      transparencyLayerAddress: transparencyLayerHash,
      recipientLayerAddress: recipientLayerHash,
      linksLayerAddress: linksLayerHash,
      creationTimestamp: new Date().getTime(),
      files: {}
    });

    const publicLayerContent = Buffer.from(
      JSON.stringify(publicLayer.toDTO())
    );
    const publicLayerHash = this.encryptionService.hash(publicLayerContent);
    const publicLayerUploadResult = await this.fileStorageService.upload({
      checksum: publicLayerHash,
      content: publicLayerContent,
      type: "text/plain",
      name: publicLayerHash
    });
    publicLayerUploadResult.unwrap();

    const certificateResult = Certificate.create({
      id: props.newCertificateId,
      publicLayer,
      publicLayerHash
    });
    const certificate = certificateResult.unwrap();

    const createCertificateBlockchainResult =
      await this.blockchainService.createCertificate(
        ownerAccount.privateKey.toString(),
        {
          id: certificate.id.toString(),
          code: certificate.props.publicLayer.code,
          version: certificate.props.publicLayer.version,
          parent: parentPublicLayer.id.toString(),
          creationTimestamp: certificate.props.publicLayer.creationTimestamp,
          publicLayerHash: certificate.props.publicLayerHash,
          certificateHash: this.encryptionService.hash(certificate.toDTO())
        }
      );

    if (createCertificateBlockchainResult.isFailure) {
      createCertificateBlockchainResult.throw();
    }

    return Result.ok(certificate);
  }

  async grantAccessToKeysLayer(
    keysLayerAddress: string,
    ownerAccount: EthereumAccount,
    newOwnerPublicKey: string
  ): Promise<Result<string>> {
    const keysLayerResult = await this.bus.execute<KeysLayer>(
      new GetCertificateKeysLayerQuery(
        keysLayerAddress,
        ownerAccount.privateKey
      )
    );
    const keysLayer = keysLayerResult.unwrap();
    const keysLayerHash = await this.createKeysLayer(
      keysLayer,
      Buffer.from(newOwnerPublicKey)
    );

    return Result.ok(keysLayerHash);
  }

  async certifyCertificate(
    props: CertifyCertificateProps
  ): Promise<Result<Certificate>> {
    const parentPublicLayerResult = await this.bus.execute<PublicLayer>(
      new GetCertificatePublicLayerQuery(props.parentPublicLayer)
    );
    const parentPublicLayer = parentPublicLayerResult.unwrap();

    const parentKeysLayerResult = await this.bus.execute<KeysLayer>(
      new GetCertificateKeysLayerQuery(
        props.parentKeysLayerHash,
        props.certifierAccount.privateKey
      )
    );
    const parentKeysLayer = parentKeysLayerResult.unwrap();

    const parentTransparencyLayerResult =
      await this.bus.execute<TransparencyLayer>(
        new GetCertificateTransparencyLayerQuery(
          parentPublicLayer.transparencyLayerAddress,
          parentKeysLayer.transparencyEncryptionKey
        )
      );
    const parentTransparencyLayer = parentTransparencyLayerResult.unwrap();

    const parentRecipientLayerResult = await this.bus.execute<RecipientLayer>(
      new GetCertificateRecipientLayerQuery(
        parentPublicLayer.recipientLayerAddress,
        parentKeysLayer.recipientEncryptionKey
      )
    );
    const parentRecipientLayer = parentRecipientLayerResult.unwrap();

    const keysLayer = await this.keysLayerFactory.create();
    const certifierKeysLayerHash = await this.createKeysLayer(
      keysLayer,
      props.certifierAccount.publicKey
    );
    const recipientKeysLayerHash = await this.createKeysLayer(
      keysLayer,
      parentRecipientLayer.publicKey
    );
    const recipientLayerHash = parentPublicLayer.recipientLayerAddress;

    const linksLayerResult = await this.linksLayerFactory.create([
      {
        id: parentPublicLayer.id.toString(),
        undisclosed: false
      }
    ]);
    const linksLayerHash = await this.encryptLayer(
      linksLayerResult.unwrap(),
      keysLayer.linksKey
    );

    const transparencyLayer = new TransparencyLayer({
      id: null,
      metadata: {},
      files: {},
      upstream: []
    });
    const transparencyLayerHash = await this.encryptLayer(
      transparencyLayer,
      keysLayer.transparencyEncryptionKey
    );
    const privateLayerHash = parentPublicLayer.privateLayerAddress;

    const publicLayer = new PublicLayer({
      id: props.newCertificateId,
      code: props.newCertificateCode,
      version: parentPublicLayer.version + 1,
      type: parentPublicLayer.type,
      owner: {
        id: props.certifierAccount.props.address.toString(),
        keys: certifierKeysLayerHash,
        publicKey: props.certifierAccount.publicKey
      },
      recipient: {
        keys: recipientKeysLayerHash
      },
      action: CERTIFICATE_ACTIONS.CERTIFIED,
      parent: parentPublicLayer.id,
      metadata: {},
      materials: [],
      privateLayerAddress: privateLayerHash,
      transparencyLayerAddress: transparencyLayerHash,
      recipientLayerAddress: recipientLayerHash,
      linksLayerAddress: linksLayerHash,
      creationTimestamp: new Date().getTime(),
      files: {}
    });

    const publicLayerContent = Buffer.from(
      JSON.stringify(publicLayer.toDTO())
    );
    const publicLayerHash = this.encryptionService.hash(publicLayerContent);
    const publicLayerUploadResult = await this.fileStorageService.upload({
      checksum: publicLayerHash,
      content: publicLayerContent,
      type: "text/plain",
      name: publicLayerHash
    });
    publicLayerUploadResult.unwrap();

    const certificateResult = Certificate.create({
      id: props.newCertificateId,
      publicLayer,
      publicLayerHash
    });
    const certificate = certificateResult.unwrap();

    const createCertificateBlockchainResult =
      await this.blockchainService.createCertificate(
        props.certifierAccount.privateKey.toString(),
        {
          id: certificate.id.toString(),
          code: certificate.props.publicLayer.code,
          version: certificate.props.publicLayer.version,
          parent: parentPublicLayer.id.toString(),
          creationTimestamp: certificate.props.publicLayer.creationTimestamp,
          publicLayerHash: certificate.props.publicLayerHash,
          certificateHash: this.encryptionService.hash(certificate.toDTO())
        }
      );

    if (createCertificateBlockchainResult.isFailure) {
      createCertificateBlockchainResult.throw();
    }

    return Result.ok(certificate);
  }

  async getCertificate(
    certificateId: CertificateId
  ): Promise<Result<Certificate>> {
    const result = await this.blockchainService.getCertificate(
      certificateId.id.toString()
    );
    const blockchainCertificate = result.unwrap();
    const publicLayerContentResult = await this.fileStorageService.download(
      blockchainCertificate.publicLayerHash
    );
    const publicLayerDTO: PublicLayerDTO = JSON.parse(
      publicLayerContentResult.unwrap().toString()
    );

    const publicLayer = new PublicLayer({
      id: CertificateId.create(
        new UniqueDomainEntityID(publicLayerDTO.id)
      ).unwrap(),
      code: publicLayerDTO.code,
      version: publicLayerDTO.version,
      type: publicLayerDTO.type as CERTIFICATE_TYPES,
      action: publicLayerDTO.action as CERTIFICATE_ACTIONS,
      owner: {
        id: publicLayerDTO.owner.id,
        publicKey: Buffer.from(publicLayerDTO.owner.publicKey),
        keys: publicLayerDTO.owner.keys
      },
      recipient: publicLayerDTO.recipient,
      parent: publicLayerDTO.parent
        ? CertificateId.create(
          new UniqueDomainEntityID(publicLayerDTO.parent)
        ).unwrap()
        : null,
      metadata: publicLayerDTO.metadata,
      materials: publicLayerDTO.materials,
      privateLayerAddress: publicLayerDTO.layers.private,
      transparencyLayerAddress: publicLayerDTO.layers.transparency,
      recipientLayerAddress: publicLayerDTO.layers.recipient,
      linksLayerAddress: publicLayerDTO.layers.links,
      creationTimestamp: publicLayerDTO.creation_timestamp,
      files: publicLayerDTO.files
    });

    return Certificate.reconstitute({
      id: publicLayer.id,
      publicLayer: publicLayer,
      publicLayerHash: blockchainCertificate.publicLayerHash
    });
  }

  async getCertificatesStorageAddress(): Promise<Result<string>> {
    return this.blockchainService.getCertificatesStorageAddress();
  }

  private async prepareFiles(
    files: {
      [key: string]:
        | InMemoryFile
        | FileReference
        | Promise<InMemoryFile | FileReference>;
    },
    encryptionKey?: SymetricEncryptionKey
  ) {
    const fileReferences: { [key: string]: FileReference } = {};

    await Promise.all(
      Object.keys(files).map(async (hash) => {
        const file: any = await files[hash];
        let content = file.content;

        if (Buffer.isBuffer(content) && encryptionKey) {
          content = await this.encryptionService.encryptWithPassword(
            file.content,
            encryptionKey
          );
        }

        const checksum = this.encryptionService.hash(content);

        await this.fileStorageService.upload({
          checksum,
          content,
          type: encryptionKey ? "text/plain" : file.type,
          name: checksum
        });

        fileReferences[checksum] = {
          name: file.name,
          type: file.type,
          checksum
        };
      })
    );

    return fileReferences;
  }

  private async encryptLayer(
    layer: RecipientLayer | TransparencyLayer | LinksLayer | PrivateLayer,
    key: SymetricEncryptionKey
  ) {
    const layerContent = await this.encryptionService.encryptWithPassword(
      Buffer.from(JSON.stringify(layer.toDTO())),
      key
    );
    const layerHash = this.encryptionService.hash(layerContent);
    const layerUploadResult = await this.fileStorageService.upload({
      checksum: layerHash,
      content: layerContent,
      type: "text/plain",
      name: layerHash
    });
    layerUploadResult.unwrap();
    return layerHash;
  }

  private async createKeysLayer(keysLayer: KeysLayer, publicKey: Buffer) {
    const keysLayerContent = await this.encryptionService.encryptWithPublicKey(
      Buffer.from(JSON.stringify(keysLayer.toDTO())),
      publicKey
    );
    const keysLayerHash = this.encryptionService.hash(keysLayerContent);
    const keysLayerUploadResult = await this.fileStorageService.upload({
      checksum: keysLayerHash,
      content: keysLayerContent,
      type: "text/plain",
      name: keysLayerHash
    });
    keysLayerUploadResult.unwrap();
    return keysLayerHash;
  }
}
