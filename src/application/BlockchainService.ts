import { Result } from "../core/Result";

export interface BlockchainCertificate {
  id: string;
  code: string;
  version: number;
  parent: string;
  creationTimestamp: number;
  publicLayerHash: string;
  certificateHash: string;
}

export interface BlockchainServiceInterface {
  createCertificate(
    privateKey: string,
    certificate: BlockchainCertificate
  ): Promise<
    Result<{
      transactionHash: string;
      blockHash: string;
    }>
  >;
  getCertificate(
    certificateId: string
  ): Promise<Result<BlockchainCertificate>>;
  getCertificatesStorageAddress(): Promise<Result<string>>;
}
