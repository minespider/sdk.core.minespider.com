import { Result } from "../core/Result";
import { UseCase } from "../core/UseCase";

export interface Request {
  getName(): string;
}

export interface Command extends Request {}
export interface Query extends Request {}

export interface Bus {
  addUseCase<Response>(
    name: string,
    useCase: UseCase<Request, Response>
  ): Result<Response>;
  execute<Response>(request: Request): Promise<Result<Response>>;
}
